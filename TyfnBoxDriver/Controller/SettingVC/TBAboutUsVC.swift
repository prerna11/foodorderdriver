//
//  TBAboutUsVC.swift
//  TyfnBox
//
//  Created by C100-11 on 18/01/18.
//  Copyright © 2018 C100-11. All rights reserved.
//

import UIKit

class TBAboutUsVC: UIViewController,UIWebViewDelegate {

    // MARK: - Variables
    var isAboutUs:Bool!
    // MARK: - IBoutlets
    @IBOutlet var webView: UIWebView!
    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        if(isAboutUs){
            SET_TRACK_EVENT(EVENT: ABOUT_US_SCREEN)
            self.title = "About Us"
        }
        else{
            self.title = "Terms & Conditions"
            SET_TRACK_EVENT(EVENT: TERMS_AND_CONDITION_SCREEN)
        }
        self.setNavigationMenu()
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.loadWebView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Delegate/Datasource
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        HIDEPROGRESS()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        HIDEPROGRESS()
        self.webView.loadRequest(NSURLRequest(url: NSURL(string: ABOUT_US_WEB_URL)! as URL) as URLRequest)
    }
//    // MARK: - User Functions
//    func loadWebView(){
//        SHOWPROGRESS()
//        var requestObject:URLRequest!
//        if(isAboutUs){
//            requestObject = URLRequest(url: (URL(string: ABOUT_US_WEB_URL))!)
//        }
//        else{
//            requestObject = URLRequest(url: (URL(string: TERMS_AND_CONDITION_URL))!)
//        }
//        webView.delegate = self
//        webView.allowsInlineMediaPlayback = true
//        webView.mediaPlaybackAllowsAirPlay = true
//        webView.mediaPlaybackRequiresUserAction = false
//        webView.loadRequest(requestObject)
//    }
    
    
    // MARK: - User Functions
    func loadWebView(){
        SHOWPROGRESS()
        var requestObject:URLRequest!
        if(isAboutUs)
        {
            if ABOUT_US_WEB_URL.isValid()
            {
                requestObject = URLRequest(url: (URL(string: ABOUT_US_WEB_URL))!)
                
                webView.delegate = self
                webView.allowsInlineMediaPlayback = true
                webView.mediaPlaybackAllowsAirPlay = true
                webView.mediaPlaybackRequiresUserAction = false
                webView.loadRequest(requestObject)
            }
            else
            {
                HIDEPROGRESS()
                let alert = UIAlertController(title: "", message: ABOUT_US_WEB_URL_MESSAGE, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel) { action in
                    // perhaps use action.title here
                    alert.dismiss(animated: true, completion: nil)
                })
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            if ABOUT_US_WEB_URL.isValid()  // != nil
            {
                requestObject = URLRequest(url: (URL(string: TERMS_AND_CONDITION_URL))!)
                
                webView.delegate = self
                webView.allowsInlineMediaPlayback = true
                webView.mediaPlaybackAllowsAirPlay = true
                webView.mediaPlaybackRequiresUserAction = false
                webView.loadRequest(requestObject)
            }
            else
            {
                HIDEPROGRESS()
                let alert = UIAlertController(title: "", message: ABOUT_US_WEB_URL_MESSAGE, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel) { action in
                    // perhaps use action.title here
                    alert.dismiss(animated: true, completion: nil)
                })
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - Button Actions

    
    // MARK: - Navigation
    func setNavigationMenu(){
        let left = UIBarButtonItem(image: UIImage(named: "ic_BackBlue"), style: .plain, target: self, action: #selector(self.leftClick)) // action:#selector(Class.MethodName) for swift 3
        left.tintColor = ThemeColor
        self.navigationItem.leftBarButtonItem  = left
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:ThemeColor]
    }
    @objc func leftClick(){
        self.navigationController?.popViewController(animated: true)
    }
}
