//
//  SettingVC.swift
//  TyfnBoxDriver
//
//  Created by c196 on 18/12/17.
//

import UIKit

class SettingVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
        
    // MARK: - Variables
    var arrayGeneralSettingOption = [String]()
    var arrayOtherSettingOption = ["About Us", "Terms and Conditions"]
    
    // MARK: - IBoutlets
    // MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        SET_TRACK_EVENT(EVENT: SETTING_SCREEN)
        self.arrayGeneralSettingOption = ["Edit Profile","Change Password","Payment Details"]
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        setNavigationMenu()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Delegate / Datasources
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.arrayGeneralSettingOption.count
        default:
            return self.arrayOtherSettingOption.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "idSettingCell") as! SettingCell
        switch indexPath.section {
        case 0:
            cell.lblSettingOption.text = self.arrayGeneralSettingOption[indexPath.row]
        default:
            cell.lblSettingOption.text = self.arrayOtherSettingOption[indexPath.row]
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            if(indexPath.row == 0){
                let editProfileVC = mainStoryboard.instantiateViewController(withIdentifier: "idEditProfileVC") as! EditProfileVC
                self.navigationController?.pushViewController(editProfileVC, animated: true)
            }
            if(indexPath.row == 1){
                let changePasswordVC = settingStoryboard.instantiateViewController(withIdentifier: "idchangePasswordVC") as!  ChangePasswordVC
                self.navigationController?.pushViewController(changePasswordVC, animated: true)
            }
            if(indexPath.row == 2){
                let paymentDetailVC = settingStoryboard.instantiateViewController(withIdentifier: "idTBPaymentDetailVC") as!  TBPaymentDetailVC
                self.navigationController?.pushViewController(paymentDetailVC, animated: true)
            }
        default:
            if(indexPath.row == 0){
                let aboutUsVC = settingStoryboard.instantiateViewController(withIdentifier: "idTBAboutUsVC") as! TBAboutUsVC
                aboutUsVC.isAboutUs = true
                self.navigationController?.pushViewController(aboutUsVC, animated: true)
            }
            if(indexPath.row == 1){
                let aboutUsVC = settingStoryboard.instantiateViewController(withIdentifier: "idTBAboutUsVC") as! TBAboutUsVC
                aboutUsVC.isAboutUs = false
                self.navigationController?.pushViewController(aboutUsVC, animated: true)
            }
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerLabel = UILabel(frame: CGRect(x: 15, y: 15, width: 150, height: 20))
        headerLabel.font = UIFont.init(name: "Arial", size: 17.0)
        switch section {
        case 0:
            headerLabel.text = "General"
        default:
            headerLabel.text = "Other"
        }
        headerLabel.textColor = ThemeColorGrey
        let headerView = UIView()
        headerView.backgroundColor = ThemeColorDarkGrey
        headerView.addSubview(headerLabel)
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }

    func logOut(){
////        AppData.sharedInstance.removeLogoutKey()
//        UserDefaults.standard.removeObject(forKey: "userType")
        DispatchQueue.main.async {
            AppData.sharedInstance.appDelegate.setupLoginUI()
        }
    }
  
// MARK: - Button Actions
    @IBAction func btnLogOutClick(_ sender: Any) {
        let alert = UIAlertController(title: "", message: LOGOUT_CONFIRMATION_MESSAGE, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .cancel) { action in
            // perhaps use action.title here
            alert.dismiss(animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Yes", style: .default) { action in
            // perhaps use action.title here
 
            UserDefaults.standard.set(false, forKey: "Is_userlogin")
            
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.application(UIApplication.shared, didFinishLaunchingWithOptions: nil)

         self.logOut()
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation
    func setNavigationMenu(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:ThemeColor]
    }
    
}

