//
//  TBPaymentDetailTableCell.swift
//  TyfnBox
//
//  Created by C100-11 on 14/11/17.
//  Copyright © 2017 C100-11. All rights reserved.
//

import UIKit

class TBPaymentDetailTableCell: UITableViewCell {

    @IBOutlet var lblBankName: UILabel!
    @IBOutlet var lblCardNumber: UILabel!
    @IBOutlet var lblCardHolderName: UILabel!
    
    @IBOutlet var btnSelect: UIButton!
    @IBOutlet var imgCard: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
