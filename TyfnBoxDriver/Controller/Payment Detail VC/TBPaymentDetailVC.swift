//
//  TBPaymentDetailVC.swift
//  TyfnBox
//
//  Created by C100-11 on 14/11/17.
//  Copyright © 2017 C100-11. All rights reserved.
//

import UIKit

class TBPaymentDetailVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    // MARK: - Variables
    var emptyTableBool : Bool!
    let refreshControl = UIRefreshControl()
//    var chefCardListObj:[ChefCard] = [ChefCard]()
    var driverCardListObj:[DriverStripeAccount] = [DriverStripeAccount]()
    var userObj:User!
    // MARK: - IBoutlets
    @IBOutlet var tbl_PaymentDetails: UITableView!
    
    // MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        SET_TRACK_EVENT(EVENT: DRIVER_CARD_LIST_SCREEN)
        self.title = "Payment Details"
        self.tbl_PaymentDetails.tableFooterView = UIView()
        self.userObj = User.init(dictionary: AppData.sharedInstance.getLoginUser()!)
        self.automaticallyAdjustsScrollViewInsets = false
        self.setUpPullToRefresh()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.userObj = User.init(dictionary: AppData.sharedInstance.getLoginUser()!)
        self.fetchDriverPaymentCardList()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Delegate / Datasources
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(userObj != nil){
            if(self.driverCardListObj.count > 0){
                tbl_PaymentDetails.backgroundView?.isHidden = true
                return self.driverCardListObj.count
            }
            else{
                tbl_PaymentDetails.backgroundView?.isHidden = false
                if(self.emptyTableBool != nil){
                    if(self.emptyTableBool == false){
                        AppData.sharedInstance.setEmptyTableView(tableView: self.tbl_PaymentDetails, message: "No Payment Details Found", imgName: "")
                    }
                }
                return 0
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(userObj != nil){
            let cell = tableView.dequeueReusableCell(withIdentifier: "idTBPaymentDetailTableCell") as! TBPaymentDetailTableCell
            let cardObj = self.driverCardListObj[indexPath.row]
            cell.lblCardNumber.text = "**** " + String(cardObj.last4!)
            if(cardObj.object == "card"){
                cell.lblBankName.text = cardObj.name ?? "\(userObj.first_name!) \(userObj.last_name!)"
                cell.lblCardHolderName.text = cardObj.country ?? ""
            }
            else{
                cell.lblBankName.text = cardObj.bank_name ?? ""
                cell.lblCardHolderName.text = cardObj.account_holder_name ?? ""
            }
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    // MARK: - Button Actions
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnAddClick(_ sender: UIBarButtonItem) {
//        AppData.sharedInstance.showAlertMessage(title: "", Message: "In Progress task for implement Express payment sysytem.", view: self)
        if(userObj.is_stripe_account == 0){
            let addPaymentConroller = settingStoryboard.instantiateViewController(withIdentifier: "idTBChefPayment")  as! TBChefPayment
            self.navigationController?.pushViewController(addPaymentConroller, animated: true)
        }
        else{
            AppData.sharedInstance.showAlertMessage(title: "", Message: STRIPE_ACCOUNT_EXIST_MESSAGE, view: self)
        }
    }
    // MARK: - User Functions
    func setUpPullToRefresh(){
        AppData.sharedInstance.setRefreshControl(refreshController: self.refreshControl, table: tbl_PaymentDetails)
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
    }
    @objc private func refreshWeatherData(_ sender: Any) {
        // Fetch Weather Data
        fetchRefreshData()
    }
    private func fetchRefreshData() {
        DispatchQueue.main.async {
            self.fetchDriverPaymentCardList()
            self.refreshControl.endRefreshing()
        }
    }
    func fetchDriverPaymentCardList(){
        if(!self.refreshControl.isRefreshing){
            SHOWPROGRESS()
        }
        let param: NSDictionary = ["userId":userObj.id ?? 0]
        APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsGetStripeExpressAccountDetails), parameters: param) { (response, error) in
            if(error == nil){
                self.driverPaymentCardListResponse(dict: response)
            }
            else{
                HIDEPROGRESS()
                self.emptyTableBool = false
                self.tbl_PaymentDetails.reloadData()
            }
        }
    }
    func driverPaymentCardListResponse(dict:NSDictionary?){
        HIDEPROGRESS()
        if(dict != nil){
            print(dict!)
            HIDEPROGRESS()
            let status = dict?.value(forKey: STATUS) as! String
            if(status == SUCCESS){
                self.emptyTableBool = true
                let cardArr = dict?.value(forKey: "account") as! NSDictionary
                let dicExternal_accounts = cardArr.value(forKey: "external_accounts") as? NSDictionary
                if(dicExternal_accounts != nil){
                    let arrData = dicExternal_accounts?.value(forKey: "data") as? NSArray
                    if(arrData != nil){
                        self.driverCardListObj.removeAll()
                        for card in arrData!{
                            let card_obj = DriverStripeAccount.init(dictionary: card as! NSDictionary)
                            self.driverCardListObj.append(card_obj!)
                        }
                    }
                }
                self.tbl_PaymentDetails.reloadData()
            }
            else{
                HIDEPROGRESS()
                print(dict!)
                self.emptyTableBool = false
                self.tbl_PaymentDetails.reloadData()
            }
        }
        else{
            HIDEPROGRESS()
            self.emptyTableBool = false
            self.tbl_PaymentDetails.reloadData()
        }
    }
    // MARK: - Navigation

}
