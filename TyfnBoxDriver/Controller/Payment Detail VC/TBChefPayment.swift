//
//  TBChefPayment.swift
//  TyfnBox
//
//  Created by C100-11 on 25/01/18.
//  Copyright © 2018 C100-11. All rights reserved.
//

import UIKit

class TBChefPayment: UIViewController,UIWebViewDelegate {

    // MARK: - Variables
    var userObj:User!
    
    // MARK: - IBuoutlets
    @IBOutlet var webView: UIWebView!
    
    // MARK: - LifeCycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SET_TRACK_EVENT(EVENT: DRIVER_ADD_STRIPE_ACCOUNT_SCREEN)
        self.title = "Add Account"
        self.setNavigationMenu()
        self.automaticallyAdjustsScrollViewInsets = false
        self.userObj = User.init(dictionary: AppData.sharedInstance.getLoginUser()!)
//        let str = AppData.sharedInstance.getQueryStringParameter(url: "http://clientapp.narola.online/pg/TyfnBox/TyfnBox_API/StripeOAuthCode.php?code=ac_CCXUiarY61fk5rcGAaPoJyvD5HxFF9Pw&state=123", param: "code")
//        print(str ?? "")
        self.loadWebView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Delegate
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("******",webView.request?.url?.absoluteString ?? "")
        let str = AppData.sharedInstance.getQueryStringParameter(url: webView.request?.url?.absoluteString ?? "", param: "code")
        if(str != nil){
            if(str != ""){
                self.stripeOuthAPI(token: str!)
            }
            else{
                HIDEPROGRESS()
            }
        }
        else{
          HIDEPROGRESS()
        }
//        HIDEPROGRESS()
    }
    // MARK: - Button Actions
    
    // MARK: - User Functions
    func loadWebView(){
        if(userObj != nil){
            if ABOUT_US_WEB_URL.isValid()
            {
                SHOWPROGRESS()
                let requestObject = URLRequest(url: (URL(string: (EXPRESS_PAYMENT_WEB_URL+String(userObj.id!))))!)
                webView.delegate = self
                webView.allowsInlineMediaPlayback = true
                webView.mediaPlaybackAllowsAirPlay = true
                webView.mediaPlaybackRequiresUserAction = false
                webView.loadRequest(requestObject)
            }
            else
            {
                HIDEPROGRESS()
                let alert = UIAlertController(title: "", message: ABOUT_US_WEB_URL_MESSAGE, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel) { action in
                    // perhaps use action.title here
                    alert.dismiss(animated: true, completion: nil)
                })
                self.present(alert, animated: true, completion: nil)
            }
            
            
//            SHOWPROGRESS()
//            let requestObject = URLRequest(url: (URL(string: (EXPRESS_PAYMENT_WEB_URL+String(userObj.id!))))!)
//            webView.delegate = self
//            webView.allowsInlineMediaPlayback = true
//            webView.mediaPlaybackAllowsAirPlay = true
//            webView.mediaPlaybackRequiresUserAction = false
//            webView.loadRequest(requestObject)
        }
    }
    //MARK: - API call
    func stripeOuthAPI(token:String){
        let param: NSDictionary = [    "oauth_code":token,
                                       "userId":userObj.id ?? 0,
                                       "rollId":userObj.role_id ?? 0
        ]
        APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsHandelStripeOauthCode), parameters: param) { (response, error) in
            if(error == nil){
                self.stripeOuthResponse(dict: response)
            }
            else{
                print(error.debugDescription)
                HIDEPROGRESS()
            }
        }
    }
    func stripeOuthResponse(dict:NSDictionary?){
        HIDEPROGRESS()
        if(dict != nil){
            print(dict!)
            let status = dict?.value(forKey: STATUS) as! String
            if(status == SUCCESS){
                let user = self.userObj
                let updatedUserObj = user?.updateStripe(obj: user!, stat: 1)
                let dic = updatedUserObj?.dictionaryRepresentation()
                AppData.sharedInstance.setLoginUser(user: dic!)
                
//                let alert = UIAlertController(title: "", message: dict?.value(forKey: "message") as? String, preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: .cancel) { action in
//                    // perhaps use action.title here
                    self.navigationController?.popViewController(animated: true)
//                })
//                self.present(alert, animated: true, completion: nil)
            }
            else{
                print(dict!)
            }
        }
    }
    // MARK: - Navigation
    func setNavigationMenu(){
        let left = UIBarButtonItem(image: UIImage(named: "ic_BackBlue"), style: .plain, target: self, action: #selector(self.leftClick)) // action:#selector(Class.MethodName) for swift 3
        left.tintColor = ThemeColor
        self.navigationItem.leftBarButtonItem  = left
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:ThemeColor]
    }
    @objc func leftClick(){
        DispatchQueue.global(qos: .background).async {
            _ = HTTPCookie.self
            let cookieJar = HTTPCookieStorage.shared
            for cookie in cookieJar.cookies! {
                // print(cookie.name+"="+cookie.value)
                cookieJar.deleteCookie(cookie)
            }
        }
        self.navigationController?.popViewController(animated: true)
    }

}
