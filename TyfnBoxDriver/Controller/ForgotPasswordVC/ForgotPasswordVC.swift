//
//  ForgotPasswordVC.swift
//  TyfnBoxDriver
//
//  Created by c196 on 22/12/17.
//
    
import UIKit
import ACFloatingTextfield_Objc

class ForgotPasswordVC: UIViewController {
    
        // MARK: - Variables
    
        // MARK: - IBoutlets
    @IBOutlet var txtEmail: ACFloatingTextField!
    @IBOutlet weak var btnResetPass: UIButton!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgKey: UIImageView!
    // MARK: - LifeCycles
        override func viewDidLoad() {
            super.viewDidLoad()
            SET_TRACK_EVENT(EVENT: FORGOT_PASSWORD_SCREEN)
            // Do any additional setup after loading the view.
        }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setThemeUI()
    }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        // MARK: - Delegate / Datasources
        // MARK: - Button Actions
        @IBAction func btnResetPasswordClick(_ sender: Any) {
            if(txtEmail.text == ""){
                AppData.sharedInstance.showAlertMessage(title: "", Message: EMAIL_REQUIRED_VALIDATION, view: self)
            }
            else if(AppData.sharedInstance.checkEmailValidation(email: txtEmail.text!) == false){
                AppData.sharedInstance.showAlertMessage(title: "", Message: EMAIL_TEXT_VALIDATION, view: self)
            }
            else{
//                 AppData.sharedInstance.showAlertMessage(title: "", Message: "An email has been sent to provided email address.", view: self)
                self.forgotPasswordAPI()
            }
        }
        
        @IBAction func btnBackClick(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
        }
    
        // MARK: - User Functions
        func forgotPasswordAPI(){
            SHOWPROGRESS()
            let param: NSDictionary = ["email":self.txtEmail.text!]
            APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsForgotPassword), parameters: param) { (response, error) in
                if(error == nil){
                    self.manageForgotPasswordResponse(dict: response)
                }
                else{
                    HIDEPROGRESS()
                }
            }
        }

    // MARK: - Navigation
    func manageForgotPasswordResponse(dict:NSDictionary?){
            HIDEPROGRESS()
            if(dict != nil){
                let status = dict?.value(forKey: STATUS) as! String
                if(status == SUCCESS){
                    self.txtEmail.text = ""
                    AppData.sharedInstance.showAlertMessage(title: "", Message: dict?.value(forKey: "message") as! String, view: self)
                }
                else{
                    AppData.sharedInstance.showAlertMessage(title: "", Message: dict?.value(forKey: "message") as! String, view: self)
                }
            }
        }
    //MARK:-set Theme method
    func setThemeUI() {
        //set Color
        
        self.txtEmail.lineColor = ThemeColorGrey
        self.txtEmail.selectedPlaceHolderColor = ThemeColor
        self.txtEmail.selectedLineColor = ThemeColor
        self.txtEmail.textColor = ThemeColorBlack
        self.btnResetPass.setTitleColor(ThemeColorWhite, for: .normal)
        self.lblHeading.textColor = ThemeColor
        self.lblDesc.textColor = ThemeColorGrey
        
        //set Fonts
        self.txtEmail.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.btnResetPass.titleLabel?.font = UIFont(name: Theme_Font_REGULAR, size: 18)
    }
}
