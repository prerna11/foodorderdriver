//
//  OrderDetailsVC.swift
//  TyfnBoxDriver
//
//  Created by c196 on 18/12/17.
//
/*
 * 0: rejected the order
 * 1: accepted the order 
 * 9: picked up status
 * 7: delivered dish status
 */
import UIKit
import CoreLocation

class OrderDetailsVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    //MARK: - IBoutlets
    @IBOutlet var tblOrderDeatils: UITableView!
    @IBOutlet var btnPickup: UIButton!
    @IBOutlet var btnDeliverd: UIButton!
    @IBOutlet weak var btnAcceptd: UIButton!
    
    //MARK: - Variables
    var userObj:User!
    var objOrderDetail : OrderDetails! 
    var setOrderStatus : String!
    var pickupAddr:String?
    
    @IBOutlet var acceptVW: UIView!
    @IBOutlet var statusVW: UIView!
    
    @IBOutlet var deliverdVW: UIView!
    @IBOutlet var pickedUpVW: UIView!
    @IBOutlet var acceptedVW: UIView!
    
    //MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setOrderStatus = ""
        SET_TRACK_EVENT(EVENT: DRIVER_ORDER_DETAIL_SCREEN)
        // Do any additional setup after loading the view.
        setNavigationMenu()
        setStatusVW()
        tblOrderDeatils.estimatedRowHeight = 200
        tblOrderDeatils.rowHeight = UITableViewAutomaticDimension
        UserDefaults.standard.set(false, forKey: "isOnDetailsScreen")
        if(self.objOrderDetail.pickup_lat != nil){
            self.getAddressFromLatLon(pdblLatitude: String(objOrderDetail.pickup_lat!), withLongitude: String(objOrderDetail.pickup_long!))
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if(AppData.sharedInstance.getLoginUser() != nil){
            self.userObj = User.init(dictionary: AppData.sharedInstance.getLoginUser()!)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated) 
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation
    func setNavigationMenu(){
        self.navigationController?.navigationBar.isHidden = false
        let logo = UIImage(named: "navImage")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        let left = UIBarButtonItem(image: UIImage(named: "ic_BackBlue"), style: .plain, target: self, action: #selector(self.leftClick)) 
        left.tintColor = ThemeColor
        self.navigationItem.leftBarButtonItem  = left
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:ThemeColor]
    }
    
    @objc func leftClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setStatusVW(){
        if objOrderDetail.order_status! == 8 || objOrderDetail.order_status! == 9 {
            statusVW.isHidden = false
        }
        if  objOrderDetail.order_status! == 9 {
           // self.acceptedVW.backgroundColor = ThemeColor
          //  self.pickedUpVW.backgroundColor = ThemeColor
            
            self.btnAcceptd.setBackgroundImage(UIImage(named: IMG_ICON_Accepted), for: .normal)
            self.btnPickup.setBackgroundImage(UIImage(named: IMG_ICON_PickedUp), for: .normal)
            self.btnPickup.setBackgroundImage(UIImage(named: IMG_ICON_PickedUp), for: .disabled)
            
            self.btnPickup.isEnabled = false
            self.btnDeliverd.isEnabled = true
        }
    }

    //MARK: - TableView Delegate/Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 162
        case 1:
            return UITableViewAutomaticDimension
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {  
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "idOrderDetailsCell", for: indexPath) as! OrderDetailsCell
            cell.setThemeUICell1()
            SET_IMAGEURL(IMAGEVIEW: cell.imgFood, IMAGE: DISH_IMAGE_URL+(objOrderDetail.dish_image ?? ""), PLACEHOLDER: #imageLiteral(resourceName: "NoImage_Thumb"))
            cell.lblDishNama.text = objOrderDetail.dish_name
            
            cell.lblDate.text = DATESTR_WITH_MONTHNAME(STR:objOrderDetail.created_at!)
                
//            cell.lblDistance.text = "\(objOrderDetail.distance!) miles"
//            if(objOrderDetail.order_status == OrderStatusEnum.PickedUpByDriver.rawValue){
//                cell.lblDistance.text = "\((String(format: "%.2f", objOrderDetail.dis_btw_dish_customer ?? 0))) miles"
//            }
//            else{
//                let totalDistance = objOrderDetail.dis_btw_dish_driver! + objOrderDetail.dis_btw_dish_customer!
//                cell.lblDistance.text = "\((String(format: "%.2f", totalDistance))) miles"
//            }
            
            print(objOrderDetail.created_at!)
            cell.lblTime.text = TIMESTR_FROMDATE(STR: objOrderDetail.created_at!)
            cell.lblQuantity.text = String(objOrderDetail.order_quantity!)
            return cell 
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "idOrderDetailsCell2", for: indexPath) as! OrderDetailsCell
            cell.setThemeUICell2()
//            cell.lblPickUpAdd.text = objOrderDetail.pickup_address
            cell.lblPickUpAdd.text = self.pickupAddr
            cell.lblChefName.text = objOrderDetail.chef_name! + " (Chef)"
         cell.lblDropAdd.text = objOrderDetail.dropp_of_address
//            cell.lblDropAdd.text = "Station Road, Suryapur Gate, Varachha, Railway Station Area, Varachha, Surat, Gujarat 395003, India"
            cell.lblCustomerName.text = objOrderDetail.customer_name! + " (Customer)"
            
//            cell.lblPickupDistance.text = "\((String(format: "%.2f", objOrderDetail.dis_btw_dish_driver!))) miles"
//            cell.lblDropOffDistance.text = "\((String(format: "%.2f", objOrderDetail.dis_btw_dish_customer!))) miles"
            
            cell.btnPickUpAdd.addTarget(self, action:#selector(OrderDetailsVC.btnPickUpAction), for:.touchUpInside)
            cell.btnDrpoAdd.addTarget(self, action:#selector(OrderDetailsVC.btnDropAction), for:.touchUpInside)
            
            cell.btnChefCall.addTarget(self, action:#selector(OrderDetailsVC.btnChefCallAction), for:.touchUpInside)
            cell.btnCustCall.addTarget(self, action:#selector(OrderDetailsVC.btnCustCallAction), for:.touchUpInside)
            return cell 
        default:
            break
        }
        return UITableViewCell()
    }
    //MARK: - Button Actions
    @objc func btnPickUpAction(_ btn:UIButton)
    {
        SET_TRACK_EVENT(EVENT: NAVIGATE_MAP_APPLICATION)
        let pickLat = objOrderDetail.pickup_lat
        let pickLong = objOrderDetail.pickup_long
        
        let dropLat = objOrderDetail.dropp_of_lat
        let dropLong = objOrderDetail.dropp_of_long
        
        
        if(pickLat != nil && pickLong != nil && AppData.sharedInstance.getCurrentLatitude() != nil){
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
//                UIApplication.shared.openURL(URL(string:"comgooglemaps://?saddr=\(pickLat!),\(pickLong!)&daddr=\(dropLat!),\(dropLong!)&directionsmode=driving&zoom=14&views=traffic")!)
//                UIApplication.shared.openURL(URL(string:"comgooglemaps://?saddr=\(AppData.sharedInstance.getCurrentLatitude()!),\(AppData.sharedInstance.getCurrentLongitude()!)&daddr=\(pickLat!),\(pickLong!)&directionsmode=driving&zoom=14&views=traffic")!)
                let s_addr = AppData.sharedInstance.getCurrentFullLocation()?.replacingOccurrences(of: " ", with: "+")
                let d_addr = objOrderDetail.pickup_address?.replacingOccurrences(of: " ", with: "+")
                print(s_addr)
                print(d_addr)
                UIApplication.shared.openURL(URL(string:"comgooglemaps://?saddr=\(s_addr!)&daddr=\(d_addr ?? "")&directionsmode=driving&zoom=14&views=traffic")!)
            }else {
                NSLog("Can't use Google Maps");
                //Apple Maps
                    if (UIApplication.shared.canOpenURL(URL(string:"http://maps.apple.com")! )) {
//                        UIApplication.shared.openURL(URL(string:"http://maps.apple.com/?ll=\(pickLat!),\(pickLong!)")!)
                        let s_address = AppData.sharedInstance.getCurrentFullLocation()?.replacingOccurrences(of: " ", with: "+")
                        let d_address = objOrderDetail.pickup_address?.replacingOccurrences(of: " ", with: "+")
                        print(s_address)
                        print(d_address)
                        let url = "http://maps.apple.com/?saddr=\(s_address!)&daddr=\(d_address!)"
                        UIApplication.shared.openURL(URL(string:url)!)
//                        UIApplication.shared.openURL(URL(string:"http://maps.apple.com/?saddr=\"\(objOrderDetail.pickup_address!)\"&daddr=\"\(objOrderDetail.dropp_of_address!)\"&dirflg=r")!)
                    } else {
                        NSLog("Can't use Apple Maps");
                   }
            }
        }
    }
    
    @objc func btnDropAction(_ btn:UIButton)
    {
        SET_TRACK_EVENT(EVENT: NAVIGATE_MAP_APPLICATION)
        let dropLat = objOrderDetail.dropp_of_lat
        let dropLong = objOrderDetail.dropp_of_long
        
        let pickLat = objOrderDetail.pickup_lat
        let pickLong = objOrderDetail.pickup_long
        
        if(dropLat != nil && dropLong != nil && AppData.sharedInstance.getCurrentLatitude() != nil){
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                //                UIApplication.shared.openURL(URL(string:
                //                    "comgooglemaps://?center=\(lat!),\(long!)&zoom=14&views=traffic")!)
                
                //                UIApplication.shared.openURL(URL(string:"comgooglemaps://?saddr=\(dropLat!),\(dropLong!)&daddr=\(pickLat!),\(pickLong!)&directionsmode=driving&zoom=14&views=traffic")!)
                //                UIApplication.shared.openURL(URL(string:"comgooglemaps://?saddr=\(AppData.sharedInstance.getCurrentLatitude()!),\(AppData.sharedInstance.getCurrentLongitude()!)&daddr=\(dropLat!),\(dropLong!)&directionsmode=driving&zoom=14&views=traffic")!)
                
                let s_addr = AppData.sharedInstance.getCurrentFullLocation()?.replacingOccurrences(of: " ", with: "+")
                let d_addr = objOrderDetail.dropp_of_address?.replacingOccurrences(of: " ", with: "+")
                UIApplication.shared.openURL(URL(string:"comgooglemaps://?saddr=\(s_addr!)&daddr=\(d_addr ?? "")&directionsmode=driving&zoom=14&views=traffic")!)
            }else {
                NSLog("Can't use Google Maps");
                //Apple Maps
                if (UIApplication.shared.canOpenURL(URL(string:"http://maps.apple.com")! )) {
//                    UIApplication.shared.openURL(URL(string:"http://maps.apple.com/?ll=\(dropLat!),\(dropLong!)")!)
                    let s_address = AppData.sharedInstance.getCurrentFullLocation()?.replacingOccurrences(of: " ", with: "+")
                    let d_address = objOrderDetail.dropp_of_address?.replacingOccurrences(of: " ", with: "+")
                    let url = "http://maps.apple.com/?saddr=\(s_address!)&daddr=\(d_address!)"
                    UIApplication.shared.openURL(URL(string:url)!)
                } else {
                    NSLog("Can't use Apple Maps");
                }
            }
        }
    }
    
    @objc func btnChefCallAction(_ btn:UIButton)
    {
        print("btnChefCallAction")
        
        if(objOrderDetail.chef_phone_num == nil || objOrderDetail.chef_phone_num == "")
        {
            AppData.sharedInstance.showAlertMessage(title: "", Message: "Phone number is not available.", view: self)
            return
        }
        
        if let url = NSURL(string: "tel://\(objOrderDetail.chef_phone_num!)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
        
    }
    
    @objc func btnCustCallAction(_ btn:UIButton)
    {
        if(objOrderDetail.customer_phone_num == nil || objOrderDetail.customer_phone_num == "")
        {
            AppData.sharedInstance.showAlertMessage(title: "", Message: "Phone number is not available.", view: self)
            return
        }
        
        print("btnCustCallAction")
        if let url = NSURL(string: "tel://\(objOrderDetail.customer_phone_num!)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func btnAcceptClicked(_ sender: UIButton) {
        if(userObj != nil){
          //-------------- uncomment if condition after payment related task is done -------------//
            
//            if(userObj.is_stripe_account == 0){
//                AppData.sharedInstance.showAlertMessage(title: "", Message: STRIPE_ACCOUNT_NOT_EXIST_MESSAGE, view: self)
//            }
//            else{
                SET_TRACK_EVENT(EVENT: DRIVER_ACCEPT_ORDER)
                setOrderStatus = "1"
                setStatusOrder()
//            }
            
        }
        else{
            
        }
    }
    
    @IBAction func btnDeclineClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: Decline_CONFIRMATION_MESSAGE, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .cancel) { action in
            // perhaps use action.title here
            alert.dismiss(animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Yes", style: .default) { action in
            SET_TRACK_EVENT(EVENT: DRIVER_DECLINE_ORDER)
            self.setOrderStatus = "0"
            self.setStatusOrder()
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnAcceptedClicked(_ sender: UIButton) {
        // nothing to do here
//        setOrderStatus = "0"
    }
    
    @IBAction func btnPickedClicked(_ sender: UIButton) {
        SET_TRACK_EVENT(EVENT: DRIVER_PICKUP_FOOD)
        setOrderStatus = "9"
        setStatusOrder()
        self.btnPickup.setBackgroundImage(UIImage(named: IMG_ICON_PickedUp), for: .normal)
        self.btnPickup.setBackgroundImage(UIImage(named: IMG_ICON_PickedUp), for: .disabled)

        //self.acceptedVW.backgroundColor = ThemeColor
    }
    
    
    @IBAction func btnDelliverdClicked(_ sender: UIButton) {
        SET_TRACK_EVENT(EVENT: DRIVER_DELIVERED_FOOD)
        self.btnDeliverd.setBackgroundImage(UIImage(named: IMG_ICON_Delivered), for: .normal)
        setOrderStatus = "7"
        setStatusOrder()
    }
    

    func setStatusOrder()
    {
        SHOWPROGRESS()
        self.userObj = User.init(dictionary: AppData.sharedInstance.getLoginUser()!)
    
        let param: NSDictionary = [    "driverId":userObj.id ?? 0,
                                       "orderId":objOrderDetail.order_id ?? 0 ,
                                       "order_status":setOrderStatus,
                                       "access_key":kNoUsername,
                                       "secret_key":AppData.sharedInstance.getTempToken() ?? ""
        ]
        APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsDriverOrderStatus), parameters: param, completion: { (response, error) in
            if(error == nil){
                self.orderStatusResponse(dictResponse: response)
//                self.tblOrder.reloadData()
            }
            else{
                HIDEPROGRESS()
            }
        })
    }
    
    func orderStatusResponse(dictResponse: NSDictionary?){
        print(dictResponse ?? "")
        HIDEPROGRESS()
        if(dictResponse != nil){
            let status = dictResponse?.value(forKey: STATUS) as! String
            if(status == SUCCESS){
                let is_active = dictResponse?.value(forKey: USER_ACTIVE) as! Int
                if(is_active == 0){
                    AppData.sharedInstance.MANAGE_DEACTIVE_ACCOUNT_FLOW(controller: self, message: DEACTIVE_ACCOUNT_MESSAGE)
                }
                else{
                    print(status)
                    let message = dictResponse?.value(forKey: "message") as! String
                    let order = self.objOrderDetail
                    if (message == "Order pickup"){            
                       // self.pickedUpVW.backgroundColor = ThemeColor
                        self.btnPickup.setBackgroundImage(UIImage(named: IMG_ICON_PickedUp), for: .normal)
                        self.btnPickup.setBackgroundImage(UIImage(named: IMG_ICON_PickedUp), for: .disabled)
                        self.btnPickup.isEnabled = false
                        self.btnDeliverd.isEnabled = true
                        let updatedOrderObj = order?.updateOrderStatus(order: order!, status: OrderStatusEnum.PickedUpByDriver.rawValue)
                        self.objOrderDetail = updatedOrderObj
                        TABLE_ROW_RELOAD(ROW: 0, SECTION: 0, TABLE: tblOrderDeatils)
                    }else if (message == "Order accepted"){
                        statusVW.isHidden = false
                        let updatedOrderObj = order?.updateOrderStatus(order: order!, status: OrderStatusEnum.AcceptedByDriver.rawValue)
                        self.objOrderDetail = updatedOrderObj
                        TABLE_ROW_RELOAD(ROW: 0, SECTION: 0, TABLE: tblOrderDeatils)
                    }else if (message == "Order delivered"){
                        let updatedOrderObj = order?.updateOrderStatus(order: order!, status: OrderStatusEnum.Delivered.rawValue)
                        self.objOrderDetail = updatedOrderObj
                        UserDefaults.standard.set(true, forKey: "isOnDetailsScreen")
                        UserDefaults.standard.set(true, forKey: "is_refreshCurrent")
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
            else{
                AppData.sharedInstance.showAlertMessage(title: "", Message: FAILED_MESSAGE, view: self)
            }
            
        }
    }
    //MARK: - Get Pikcup Address from lat,long
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        SHOWPROGRESS()
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        var addressString : String = ""
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        SHOWPROGRESS()
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    HIDEPROGRESS()
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                else{
                    let pm = placemarks! as [CLPlacemark]
                    
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality!
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + ", " + pm.thoroughfare!
                        }
                        if pm.locality != nil {
                            addressString = addressString + ", " + pm.locality!
                        }
                        self.pickupAddr = addressString
                        TABLE_ROW_RELOAD(ROW: 1, SECTION: 0, TABLE: self.tblOrderDeatils)
                        HIDEPROGRESS()
                    }
                    else{
                        HIDEPROGRESS()
                        TABLE_ROW_RELOAD(ROW: 1, SECTION: 0, TABLE: self.tblOrderDeatils)
                    }
                }
        })
    }
}
