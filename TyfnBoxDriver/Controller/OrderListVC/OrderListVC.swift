//
//  OrderListVC.swift
//  TyfnBoxDriver
//
//  Created by c196 on 18/12/17.
//

import UIKit
import KRPullLoader

class OrderListVC: UIViewController,UITableViewDataSource,UITableViewDelegate,KRPullLoadViewDelegate {

    //MARK:- Property
    var emptyTblBool :Bool!
    var userObj:User!
    var objOrder:OrderDetails!
    fileprivate var isResultAvailable:Bool = false
    fileprivate var isPastOrderResultAvailable:Bool = false
    let refreshView = KRPullLoadView()
    let loadMoreView = KRPullLoadView()
    let refreshControl = UIRefreshControl()
    var arrCurrentOrder:[OrderDetails] = [OrderDetails]()
    var arrPastOrder:[OrderDetails] = [OrderDetails]()
    var rightSwipeGest : UISwipeGestureRecognizer!
    var leftSwipeGest : UISwipeGestureRecognizer!
    var setOrderStatus : String!
    var objOrderDetail1 : OrderDetails!
    var curframe = CGRect()
    var set = true
    var isDisplayMSG = false
    
    // MARK: - IBoutlets
    @IBOutlet var tblOrder: UITableView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var btnCurrent: UIButton!
    @IBOutlet var lblCurrent: UILabel!
    @IBOutlet var btnPastOrder: UIButton!
    @IBOutlet var lblPast: UILabel!
    var timer:Timer!
    let notificationCenter = NotificationCenter.default

    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        set = true
        setOrderStatus = ""
        SET_TRACK_EVENT(EVENT: DRIVER_ORDER_SCREEN)
        setUpNavigationBar()
         self.setUpPullToRefresh()
        getcurrentOrder(lastId: 0)
        refreshView.delegate = self
        loadMoreView.delegate = self
        tblOrder.addPullLoadableView(loadMoreView, type: .loadMore)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5) {
            self.tblOrder.removePullLoadableView(self.refreshView)
        }
        
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
        
       UserDefaults.standard.set(false, forKey: "isOnDetailsScreen")
    }
    
    @objc func appMovedToForeground() {
        print("App moved to Foreground!")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
            self.getcurrentOrder(lastId: 0)
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setThemeUI()
        let check=UserDefaults.standard.bool(forKey: "isOnDetailsScreen")
        if check == false
        {
            set = true
            self.lblPast.isHidden = true
            self.btnCurrent.setTitleColor(ThemeColor, for: .normal)
            self.lblCurrent.isHidden = false
            self.btnPastOrder.setTitleColor(ThemeColorGrey, for: .normal)
            refresh()
        }else{
            set = false
            self.lblPast.isHidden = false
            self.btnPastOrder.setTitleColor(ThemeColor, for: .normal)
            self.lblCurrent.isHidden = true
            self.btnCurrent.setTitleColor(ThemeColorGrey, for: .normal)
            getPastOrder(lastId: 0)
        }
         timer = Timer.scheduledTimer(timeInterval: TimeInterval(TIME_INTERVAL), target: self, selector:  #selector(refresh), userInfo: nil, repeats: true)
    }
        
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    deinit {
        tblOrder.removePullLoadableView(refreshView)
        tblOrder.removePullLoadableView(loadMoreView)
        NotificationCenter.default.removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavigationBar() {
        self.navigationController?.navigationBar.isHidden = false
        let logo = UIImage(named: IMG_ICON_NAV_LOGO)
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        self.navigationController?.navigationBar.tintColor = ThemeColor
    }
    
    //MARK:- pull to refresh
    @objc func refresh(){
        if set == true{
            if(self.arrCurrentOrder.count > 0){
                let lastDish = self.arrCurrentOrder[self.arrCurrentOrder.count - 1]
                self.fetchRefreshCurrentOrder(lastId: lastDish.order_id ?? 0)
            }
            else{
                self.tblOrder.reloadData()
            }
        }
    }
    
    func fetchRefreshCurrentOrder(lastId:Int){
        let param: NSDictionary = [
             "driverId":userObj.id ?? 0,
            "offset":arrCurrentOrder.count,
            "latitude":AppData.sharedInstance.getCurrentLatitude() ?? "",
            "longitude":AppData.sharedInstance.getCurrentLongitude() ?? "",
            "access_key":kNoUsername,
            "secret_key":AppData.sharedInstance.getTempToken() ?? ""]
        print(param)
        APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsDriverCurrentOrderRefresh), parameters: param) { (response, error) in
            if(error == nil){
                self.manageRefreshCurrentOrdert(dict: response)
            }
            else{
                HIDEPROGRESS()
            }
        }
    }
    
    func manageRefreshCurrentOrdert(dict:NSDictionary?){
        HIDEPROGRESS()
        if(dict != nil){
            print(dict!)
            let status = dict?.value(forKey: STATUS) as! String
            if(status == SUCCESS){
                let is_active = dict?.value(forKey: USER_ACTIVE) as! Int
                if(is_active == 0){
                    AppData.sharedInstance.MANAGE_DEACTIVE_ACCOUNT_FLOW(controller: self, message: DEACTIVE_ACCOUNT_MESSAGE)
                }
                else{
                    self.arrCurrentOrder.removeAll()
                    isResultAvailable = true
                      arrCurrentOrder = OrderDetails.modelsFromDictionaryArray(array: dict?.value(forKey: "OrderDetails") as! NSArray) 
                    self.tblOrder.reloadData()
                    //                let indexpath = NSIndexPath(row: self.dishObjArr.count-1, section: 0)
                }
            }
            else{
                isResultAvailable = false
                self.tblOrder.reloadData()
            }
        }
    }

    func setUpPullToRefresh(){
        AppData.sharedInstance.setRefreshControl(refreshController: self.refreshControl, table: tblOrder)
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
    }
    
    @objc private func refreshWeatherData(_ sender: Any) {
        // Fetch Weather Data
        fetchRefreshData()
    }
    private func fetchRefreshData() {
        DispatchQueue.main.async {
            if self.set == true {
                self.getcurrentOrder(lastId: 0)
            }else{
                self.getPastOrder(lastId: 0)
            }
            self.refreshControl.endRefreshing()
        }
    }
    
    // MARK: - KRPullLoadView delegate -------------------
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .loading(completionHandler):
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                    completionHandler()
                    var order:OrderDetails?
                    if self.set == true 
                    {
                        if(self.isResultAvailable){
                            if(self.arrCurrentOrder.count > 0){
                                order = self.arrCurrentOrder[self.arrCurrentOrder.count-1]
//                                self.btnPast.isEnabled = false
                            }
                        }
                        else{
//                            self.btnPast.isEnabled = true
                        }
                    }
                    else{
                        if(self.isPastOrderResultAvailable){
                            if(self.arrPastOrder.count > 0){
                                order = self.arrPastOrder[self.arrPastOrder.count-1]
//                                self.btnPending.isEnabled = false
                            }
                        }
                        else{
//                            self.btnPending.isEnabled = true
                        }
                    }
                    if(order != nil){
                        if self.set == true 
                        {
                            self.getcurrentOrder(lastId: (order?.order_id)!)
                        }else{ 
                        self.getPastOrder(lastId: (order?.order_id)!)
                        }
                    }
                }
            default: break
            }
            return
        }
    }
    
    //MARK:- wsCurrentOrder API Calling 
    func getcurrentOrder(lastId:Int)
    {
        if(!self.refreshControl.isRefreshing){
            if(lastId == 0){
                SHOWPROGRESS()
            }
        }        
       
        self.userObj = User.init(dictionary: AppData.sharedInstance.getLoginUser()!)
        let param: NSDictionary = [    "driverId":userObj.id ?? 0,
                                       "offset":lastId,
                                       "latitude":AppData.sharedInstance.getCurrentLatitude() ?? "0.0",
                                      "longitude":AppData.sharedInstance.getCurrentLongitude() ?? "0.0",
                                       "access_key":kNoUsername,
                                       "secret_key":AppData.sharedInstance.getTempToken() ?? ""
        ]
        
        print(param)
        APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsCurrentOrder), parameters: param, completion: { (response, error) in
            if(error == nil){
                self.currentOrderResponse(dictResponse: response,lastId: lastId)
                self.tblOrder.reloadData()
            }
            else{
                self.emptyTblBool = false
                HIDEPROGRESS()
            }
        })
    }
    
    func currentOrderResponse(dictResponse: NSDictionary?,lastId:Int){
        print(dictResponse ?? "")
        HIDEPROGRESS()
        if(dictResponse != nil){
            let status = dictResponse?.value(forKey: STATUS) as! String
            if lastId == 0{
                arrCurrentOrder = [OrderDetails]()
            }
            if(status == SUCCESS){
                let is_active = dictResponse?.value(forKey: USER_ACTIVE) as! Int
                if(is_active == 0){
                    AppData.sharedInstance.MANAGE_DEACTIVE_ACCOUNT_FLOW(controller: self, message: DEACTIVE_ACCOUNT_MESSAGE)
                }
                else{
                    self.emptyTblBool = true
                    self.isResultAvailable = true
                    isDisplayMSG = false
                    let currentOrderArr = dictResponse?.value(forKey: "OrderDetails") as! NSArray
                    for order in currentOrderArr{
                        let orderO = OrderDetails.init(dictionary: order as! NSDictionary)
                        self.arrCurrentOrder.append(orderO!)
                    }
                                        
                    //arrCurrentOrder = OrderDetails.modelsFromDictionaryArray(array: dictResponse?.value(forKey: "OrderDetails") as! NSArray) 
                    self.tblOrder.reloadData()
                }
            }
            else{
                if lastId == 0{
                    arrCurrentOrder = [OrderDetails]()
                }
                AppData.sharedInstance.showAlertMessage(title: "", Message: dictResponse?.value(forKey: "message") as! String, view: self)
                self.emptyTblBool = false
                self.isResultAvailable = false
                isDisplayMSG = true
                refreshView.activityIndicator.stopAnimating()
                self.tblOrder.reloadData()
            }
        }
    }
    
    //MARK:- wsPastOrder API Calling 
    func getPastOrder(lastId:Int)
    {
        if(!self.refreshControl.isRefreshing){
            if(lastId == 0){
                SHOWPROGRESS()
            }
        }        
        
        self.userObj = User.init(dictionary: AppData.sharedInstance.getLoginUser()!)
        let param: NSDictionary = [    "driverId":userObj.id ?? 0,
                                       "last_order_id":lastId,
                                       "access_key":kNoUsername,
                                       "secret_key":AppData.sharedInstance.getTempToken() ?? ""
        ]
        APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsPastOrder), parameters: param, completion: { (response, error) in
            if(error == nil){
                self.PastOrderResponse(dictResponse: response,lastId: lastId)
                self.tblOrder.reloadData()
            }
            else{
                self.emptyTblBool = false
                HIDEPROGRESS()
            }
        })
    }
    
    func PastOrderResponse(dictResponse: NSDictionary?,lastId:Int){
        print(dictResponse ?? "")
        HIDEPROGRESS()
        if(dictResponse != nil){
            let status = dictResponse?.value(forKey: STATUS) as! String
            if lastId == 0{
                arrPastOrder = [OrderDetails]()
            }
            if(status == SUCCESS){
                let is_active = dictResponse?.value(forKey: USER_ACTIVE) as! Int
                if(is_active == 0){
                    AppData.sharedInstance.MANAGE_DEACTIVE_ACCOUNT_FLOW(controller: self, message: DEACTIVE_ACCOUNT_MESSAGE)
                }
                else{
                self.isPastOrderResultAvailable = true
                 isDisplayMSG = false
                self.emptyTblBool = true
                    let pendingOrderArr = dictResponse?.value(forKey: "OrderDetails") as! NSArray
                    for order in pendingOrderArr{
                        let orderO = OrderDetails.init(dictionary: order as! NSDictionary)
                        self.arrPastOrder.append(orderO!)
                    }
//                arrPastOrder = OrderDetails.modelsFromDictionaryArray(array: )
                }
            }
            else{
                self.isPastOrderResultAvailable = false
                isDisplayMSG = true
                self.emptyTblBool = false
                refreshView.activityIndicator.stopAnimating()
                self.tblOrder.reloadData()
            }
        }
    }
    
    //MARK:- wsDriverOrderStatus API Calling 
    func setStatusOrder()
    {
        SHOWPROGRESS()
        self.userObj = User.init(dictionary: AppData.sharedInstance.getLoginUser()!)
        let param: NSDictionary = [    "driverId":userObj.id ?? 0,
                                       "orderId":objOrderDetail1.order_id ?? 0 ,
                                       "order_status":setOrderStatus,
                                       "access_key":kNoUsername,
                                       "secret_key":AppData.sharedInstance.getTempToken() ?? ""
        ]
        APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsDriverOrderStatus), parameters: param, completion: { (response, error) in
            if(error == nil){
                self.orderStatusResponse(dictResponse: response)
                //                self.tblOrder.reloadData()
            }
            else{
                HIDEPROGRESS()
            }
        })
    }
    
    func orderStatusResponse(dictResponse: NSDictionary?){
        print(dictResponse ?? "")
        HIDEPROGRESS()
        if(dictResponse != nil){
            let status = dictResponse?.value(forKey: STATUS) as! String
            if(status == SUCCESS){
                    let is_active = dictResponse?.value(forKey: USER_ACTIVE) as! Int
                    if(is_active == 0){
                        AppData.sharedInstance.MANAGE_DEACTIVE_ACCOUNT_FLOW(controller: self, message: DEACTIVE_ACCOUNT_MESSAGE)
                    }
                    else{
                print(status)
                refresh()
                    }
            }
            else{
                let msg = dictResponse?.value(forKey: "message") as! String
                AppData.sharedInstance.showAlertMessage(title: "", Message: msg, view: self)
            }            
        }
    }
    
    //MARK:- TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if set == true {
            if arrCurrentOrder.count > 0{
                tblOrder.backgroundView?.isHidden = true
                return arrCurrentOrder.count
            }else{
                tblOrder.backgroundView?.isHidden = false
                if(self.emptyTblBool != nil){
                    if(self.emptyTblBool == false){
                        AppData.sharedInstance.setEmptyTableView(tableView: self.tblOrder, message: NOORDER_MESSAGE, imgName: "")
                    }
                }
                return 0
            }
        }else{
            if arrPastOrder.count > 0{
                tblOrder.backgroundView?.isHidden = true
                return arrPastOrder.count
            }else{
                tblOrder.backgroundView?.isHidden = false
                if(self.emptyTblBool != nil){
                    if(self.emptyTblBool == false){
                        AppData.sharedInstance.setEmptyTableView(tableView: self.tblOrder, message: NOORDER_MESSAGE, imgName: "")
                    }
                }
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {       
        let cell = tableView.dequeueReusableCell(withIdentifier: "idOrderListCell", for: indexPath) as! OrderListCell
        cell.setThemeUI()
        if set == true {
            objOrder = arrCurrentOrder[indexPath.row]
            if curframe.isEmpty
            {
                     curframe = cell.foodVW.frame
                    print(curframe)
            }
       
            rightSwipeGest = UISwipeGestureRecognizer()
            rightSwipeGest.direction = .left
            rightSwipeGest.numberOfTouchesRequired = 1
            rightSwipeGest.addTarget(self, action: #selector(OrderListVC.rightSwipeAction(_:)))
            
            cell.tag = indexPath.row + 10000
            rightSwipeGest.accessibilityHint = cell.tag .description
            cell.foodVW.addGestureRecognizer(rightSwipeGest)
            
            leftSwipeGest = UISwipeGestureRecognizer()
            leftSwipeGest.direction = .right
            leftSwipeGest.numberOfTouchesRequired = 1
            leftSwipeGest.addTarget(self, action: #selector(OrderListVC.leftSwipeAction(_:)))
            cell.tag = indexPath.row + 10000
            leftSwipeGest.accessibilityHint = cell.tag .description
            cell.foodVW.addGestureRecognizer(leftSwipeGest)
            
            cell.btnAccept.tag = indexPath.row
            cell.btnReject.tag = indexPath.row
            
            cell.btnAccept.topRoundedButton()
            cell.btnReject.BottomRoundedButton()
            
            cell.btnAccept.addTarget(self, action:#selector(OrderListVC.btnAcceptAction), for:.touchUpInside)
            cell.btnReject.addTarget(self, action:#selector(OrderListVC.btnRejectAction), for:.touchUpInside)
            
        }else{
            objOrder = arrPastOrder[indexPath.row]

        }
        if set == false {
            cell.lblDistance.text =  "Delivered"
        }else{
            cell.lblDistance.text = "\(objOrder.distance!) miles away"
        }
        
        cell.lblDishName.text = objOrder.dish_name
        cell.lblDate.text = DATESTR_WITH_MONTHNAME(STR: objOrder.created_at!) 
        
        SET_IMAGEURL(IMAGEVIEW: cell.imgFood, IMAGE: DISH_IMAGE_URL+(objOrder.dish_image ?? ""), PLACEHOLDER: #imageLiteral(resourceName: "NoImage_Thumb"))
        
       
        cell.foodVW.layer.cornerRadius = 4
        cell.foodVW.layer.masksToBounds = false
        cell.foodVW.layer.borderWidth = 0.5
        cell.foodVW.layer.borderColor = UIColor.lightGray.cgColor
        cell.foodVW.layer.cornerRadius = 4
        return cell 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           if set == true 
           {
            UserDefaults.standard.set(true, forKey: "isOnDetailsScreen")
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "idOrderDetailsVC") as! OrderDetailsVC 
            print(arrCurrentOrder.count)
            if arrCurrentOrder.count > 0{
                vc.objOrderDetail = arrCurrentOrder[indexPath.row]
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    //MARK:- button Action
    @objc func btnAcceptAction(_ btn:UIButton)
    {
        setOrderStatus = "1"
        self.objOrderDetail1 = self.arrCurrentOrder[btn.tag]
        setStatusOrder()
    }
    
    @objc func btnRejectAction(_ btn:UIButton)
    {
        let alert = UIAlertController(title: "", message: Decline_CONFIRMATION_MESSAGE, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .cancel) { action in
            // perhaps use action.title here
            alert.dismiss(animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Yes", style: .default) { action in
            self.setOrderStatus = "0"
            self.objOrderDetail1 = self.arrCurrentOrder[btn.tag]
            self.setStatusOrder()
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnCurrentClicked(_ sender: UIButton) {
        isDisplayMSG = false
        self.lblPast.isHidden = true
        self.btnCurrent.setTitleColor(ThemeColor, for: .normal)
        self.lblCurrent.isHidden = false
        self.btnPastOrder.setTitleColor(ThemeColorGrey, for: .normal)
        set = true
        if(self.arrCurrentOrder.count == 0){
            self.getcurrentOrder(lastId: 0)}
        let refreshCurrent = UserDefaults.standard.bool(forKey: "is_refreshCurrent")
        if refreshCurrent == true {
            self.getcurrentOrder(lastId: 0)
            UserDefaults.standard.set(false, forKey: "is_refreshCurrent")
        }
        self.tblOrder.reloadData()
    }
    
    @IBAction func btnPaskOrderClicked(_ sender: UIButton) {
        isDisplayMSG = false
        self.lblPast.isHidden = false
        self.btnPastOrder.setTitleColor(ThemeColor, for: .normal)
        self.lblCurrent.isHidden = true
        self.btnCurrent.setTitleColor(ThemeColorGrey, for: .normal)
        set = false
        if(self.arrPastOrder.count == 0){
            self.getPastOrder(lastId: 0)}
        self.tblOrder.reloadData()
    }
    
    @objc func leftSwipeAction(_ sender : UIGestureRecognizer)    
    {
        print("test")  
        tblOrder.reloadData()
    }
    
    @objc func rightSwipeAction(_ sender : UIGestureRecognizer)    
    {
        if set == true {
            let cnt = arrCurrentOrder.count
            
            for i in 0..<cnt
            {
                let cell = tblOrder.viewWithTag(i + 10000) as! OrderListCell
                cell.foodVW.frame = CGRect(x: 0, y: 0, width: cell.foodVW.frame.size.width, height: 90)
                cell.foodVW.layer.cornerRadius = 4
                cell.statusVW.isHidden = true
            }
            
            let cell =  tblOrder.viewWithTag(Int(sender.accessibilityHint!)!) as! OrderListCell
            print(cell.foodVW.frame.origin.x)
            if cell.foodVW.frame.origin.x == 0 
            {
                cell.statusVW.isHidden = false
                cell.foodVW.layer.cornerRadius = 0
                cell.foodVW.frame = CGRect(x: cell.foodVW.frame.origin.x - cell.statusVW.frame.size.width, y: cell.foodVW.frame.origin.y, width: cell.foodVW.frame.size.width, height: cell.foodVW.frame.size.height)
            }
        }else{
            let cnt = arrCurrentOrder.count
            for i in 0..<cnt 
            {
                let cell = tblOrder.viewWithTag(i + 10000) as! OrderListCell
                cell.foodVW.layer.cornerRadius = 4
                cell.statusVW.isHidden = true
            }
        }
    }
    
    //MARK:-set Theme method
    func setThemeUI() {
        //set Color
        self.lblPast.backgroundColor = ThemeColor
        self.lblCurrent.backgroundColor = ThemeColor
        self.btnCurrent.setTitleColor(ThemeColor, for: .normal)
        self.btnPastOrder.setTitleColor(ThemeColorGrey, for: .normal)
        
        //set Fonts
        self.btnPastOrder.titleLabel?.font = UIFont(name: Theme_Font_REGULAR, size: 15)
        self.btnCurrent.titleLabel?.font = UIFont(name: Theme_Font_REGULAR, size: 15)
    }
    
}
