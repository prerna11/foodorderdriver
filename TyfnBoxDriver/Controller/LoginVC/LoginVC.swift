//
//  LoginVC.swift
//  TyfnBoxDriver
//
//  Created by c196 on 18/12/17.
//

import UIKit
import ACFloatingTextfield_Objc

class LoginVC: UIViewController {
    
    // MARK: - IBoutlets
    @IBOutlet var txtEmail: ACFloatingTextField!
    @IBOutlet var txtPasword: ACFloatingTextField!
    @IBOutlet weak var btnforgotPass: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var lblDontHavAcc: UILabel!
    @IBOutlet weak var btnLogin: UIButton!
    
    // MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        SET_TRACK_EVENT(EVENT: LOGIN_SCREEN)
        self.navigationController?.navigationBar.isHidden = true

        //        self.txtEmail.text = "hpr@narola.email"
        //        self.txtPasword.text = "123456"
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setThemeUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Delegate / Datasources
    // MARK: - Button Actions
    @IBAction func btnLoginClick(_ sender: Any) {
        if(txtEmail.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: EMAIL_REQUIRED_VALIDATION, view: self)
        }
        else if(txtPasword.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: PASSWORD_REQUIRED_VALIDATION, view: self)
        }
        else if(!AppData.sharedInstance.checkLocationService()){
            let alert = UIAlertController(title: "", message: LOCATION_SERVICE_MESSAGE, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel) { action in
                // perhaps use action.title here
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        })
                    } else {
                        // Fallback on earlier versions
                    }
                }
            })
            self.present(alert, animated: true, completion: nil)
        }
        else{
            self.loginUser()
        }
    }
    
    // MARK: - User Functions
    func loginUser(){
        SHOWPROGRESS()
        let param: NSDictionary = ["email":self.txtEmail.text!,
                                   "password":txtPasword.text!,
                                   "deviceid":AppData.sharedInstance.getDeviceToken() ?? "",
                                   "devicetype":DEVICE_TYPE,
                                   "access_key":kNoUsername,
                                   "secret_key":AppData.sharedInstance.getTempToken() ?? ""
        ]
        APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsLoginV2), parameters: param) { (response, error) in
            if(error == nil){
                self.manageLoginResponse(dict: response)
            }
            else{
                HIDEPROGRESS()
            }
        }
    }
    
    func manageLoginResponse(dict:NSDictionary?){
        HIDEPROGRESS()
        if(dict != nil){
            print(dict!)
            let status = dict?.value(forKey: STATUS) as! String
            if(status == SUCCESS){
//                let is_active = dict?.value(forKey: USER_ACTIVE) as! Int
//                if(is_active == 0){
//                    AppData.sharedInstance.MANAGE_DEACTIVE_ACCOUNT_FLOW(controller: self, message: DEACTIVE_ACCOUNT_MESSAGE)
//                }
//                else{
                    let userToken = dict?.value(forKey: "usertoken") as! String
                    AppData.sharedInstance.setUserToken(token: userToken)
                    let userDict = dict?.value(forKey: "user") as! NSDictionary
                let userobj = User.init(dictionary: userDict)
                    AppData.sharedInstance.setLoginUser(user: userDict)
                SET_USER_IDENTIFY(USERID: (userobj?.id!)!)
    //                let userobj = User.init(dictionary: userDict)
                    UserDefaults.standard.set(true, forKey: "Is_userlogin")
                    let orderVC = mainStoryboard.instantiateViewController(withIdentifier: "idHomeNavigation") as! UINavigationController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = orderVC
                    appDelegate.window?.makeKeyAndVisible()
//                }
            }
            else{
                AppData.sharedInstance.showAlertMessage(title: "", Message: dict?.value(forKey: "message") as! String, view: self)
            }
        }
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    @IBAction func btnForgotPasswordClick(_ sender: Any) {
        let forgotPwdVC = mainStoryboard.instantiateViewController(withIdentifier: "idForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(forgotPwdVC, animated: true)
    }
    
    @IBAction func btnSignUpClick(_ sender: Any) {
        let SignupVc = mainStoryboard.instantiateViewController(withIdentifier: "idSignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(SignupVc, animated: true)
    }
    
    //MARK:-set Theme method
    func setThemeUI() {
        //set Color
        self.txtEmail.lineColor = ThemeColorGrey
        self.txtPasword.lineColor = ThemeColorGrey
        
        self.txtEmail.selectedPlaceHolderColor = ThemeColor
        self.txtPasword.selectedPlaceHolderColor = ThemeColor
        
        self.txtEmail.selectedLineColor = ThemeColor
        self.txtPasword.selectedLineColor = ThemeColor
        
        self.txtEmail.textColor = ThemeColorBlack
        self.txtPasword.textColor = ThemeColorBlack
        self.lblDontHavAcc.textColor = ThemeColorBlack
        
        self.btnforgotPass.setTitleColor(ThemeColorGrey, for: .normal)
        self.btnSignUp.setTitleColor(ThemeColor, for: .normal)
        self.btnforgotPass.setTitleColor(ThemeColorGrey, for: .highlighted)
        self.btnSignUp.setTitleColor(ThemeColor, for: .highlighted)
        self.btnforgotPass.setTitleColor(ThemeColorGrey, for: .selected)
        self.btnSignUp.setTitleColor(ThemeColor, for: .selected)
        
        //set Fonts
        self.txtPasword.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtEmail.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.lblDontHavAcc.font = UIFont(name: Theme_Font_REGULAR, size: 15)
        self.btnforgotPass.titleLabel?.font = UIFont(name: Theme_Font_REGULAR, size: 15)
        self.btnSignUp.titleLabel?.font = UIFont(name: Theme_Font_REGULAR, size: 14)
    }
}
