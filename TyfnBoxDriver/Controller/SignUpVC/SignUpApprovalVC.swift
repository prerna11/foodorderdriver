//
//  SignUpApprovalVC.swift
//  TyfnBoxDriver
//
//  Created by c196 on 05/01/18.
//

import UIKit

class SignUpApprovalVC: UIViewController {

    //MARK:-Outlets
    
    @IBOutlet weak var imgWaitinApproval: UIImageView!
    @IBOutlet weak var btnThankYou: UIButton!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    //Mark:-Variables
    
    //MARK:-LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        SET_TRACK_EVENT(EVENT: APPROVAL_SCREEN)
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setThemeUI()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnThankYouClick(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: false)
//        let LoginVc = mainStoryboard.instantiateViewController(withIdentifier: "idTBLoginVC") as! LoginVC
//        self.navigationController?.pushViewController(LoginVc, animated: false)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:-set Theme method
    func setThemeUI() {
        //set Color
        self.lblHeading.textColor = ThemeColor
        self.lblDesc.textColor = ThemeColorGrey
        self.lblFrom.textColor = ThemeColor
        self.btnThankYou.setTitleColor(ThemeColorWhite, for: .normal)
        
        //set Fonts
        self.lblHeading.font = UIFont(name: Theme_Font_REGULAR, size: 22)
        self.lblDesc.font = UIFont(name: Theme_Font_REGULAR, size: 14)
        self.lblHeading.font = UIFont(name: Theme_Font_REGULAR, size: 14)
        self.btnThankYou.titleLabel?.font = UIFont(name: Theme_Font_REGULAR, size: 18)
    }
}
