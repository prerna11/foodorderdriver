//
//  SignUpVC.swift
//  TyfnBoxDriver
//
//  Created by c196 on 18/12/17.
//

import UIKit
import ACFloatingTextfield_Objc
import CoreLocation

class SignUpVC: UIViewController,SelectImageDelegate,UITextFieldDelegate {
    
    // MARK: - Variables
    var userType:Int!
    var profileImage:UIImage!
    var address = ""
    
    // MARK: - IBoutlets
    @IBOutlet var txtFName: ACFloatingTextField!
    @IBOutlet var txtLName: ACFloatingTextField!
    @IBOutlet var txtEmail: ACFloatingTextField!
    @IBOutlet var txtPhoneNo: ACFloatingTextField!
    @IBOutlet var txtLicenseNumber: ACFloatingTextField!    
    @IBOutlet var txtPassword: ACFloatingTextField!
    @IBOutlet var txtConfirmPassword: ACFloatingTextField!
    
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var imgProfile: UIImageView!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    // MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        SET_TRACK_EVENT(EVENT: SIGNUP_SCREEN)
        // Do any additional setup after loading the view.
        if(AppData.sharedInstance.getCurrentLatitude() != nil){
            self.getAddressFromLatLon(pdblLatitude: AppData.sharedInstance.getCurrentLatitude()!, withLongitude: AppData.sharedInstance.getCurrentLongitude()!)
        }
        
        self.setUpTextField()
        AppData.sharedInstance.roundAvatarImage(imageview: self.imgProfile)
        //AppData.sharedInstance.setImageBorder(view: self.imgProfile)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setThemeUI()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TextField Delegate / Datasources
    func textFieldDidEndEditing(_ textField: UITextField) {
        let str = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if(str == nil || str == ""){
            textField.text = ""
        }
    }
    
    // MARK: - Select Image Delegate
    func didFinishSelectImage(image: UIImage?, videoURL: NSURL?) {
        self.dismiss(animated: false, completion: nil)
        if(image != nil){
            self.profileImage = image
            self.imgProfile.image = image
        }
    }
    
    // MARK: - Button Actions        
    @IBAction func btnProfileClick(_ sender: UIButton) {
        let SelectImagePopup = mainStoryboard.instantiateViewController(withIdentifier: "idTBPickImageDelegateVC") as! TBPickImageDelegateVC
        SelectImagePopup.modalPresentationStyle = .overFullScreen
        SelectImagePopup.delegate = self
        SelectImagePopup.view_title = "Select Profile Photo"
        SelectImagePopup.fromProfile = true
        SelectImagePopup.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        SelectImagePopup.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            SelectImagePopup.view.alpha = 1.0
            SelectImagePopup.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
        self.present(SelectImagePopup, animated: false, completion: nil)
    }
    
    @IBAction func btnSignInClicked(_ sender: UIButton) {
        if (self.txtEmail.text != "" || self.txtFName.text != "" || self.txtLName.text != "" || self.txtPhoneNo.text != "" || self.txtPassword.text != "" || self.txtConfirmPassword.text != ""){
            
            let alert = UIAlertController(title: "Alert", message:"Details which is entered will get lost. Are you sure you want to exit?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
                // perhaps use action.title here
                self.navigationController?.popViewController(animated: false)
            })
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
                // perhaps use action.title here
            })
            self.present(alert, animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnSignUpClick(_ sender: Any) {
        self.view.endEditing(true)
        if(txtFName.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: FNAME_REQUIRED_VALIDATION, view: self)
        }
        else if(txtLName.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: LNAME_REQUIRED_VALIDATION, view: self)
        }
        else if(txtEmail.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: EMAIL_REQUIRED_VALIDATION, view: self)
        }
        else if(AppData.sharedInstance.checkEmailValidation(email: txtEmail.text!) == false){
            AppData.sharedInstance.showAlertMessage(title: "", Message: EMAIL_TEXT_VALIDATION, view: self)
        }
        else if(txtPhoneNo.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: PHONE_REQUIRED_VALIDATION, view: self)
        }
        else if(txtPhoneNo.text != "" && ((txtPhoneNo.text?.count)! < 6 || (txtPhoneNo.text?.count)! > 15)){
            AppData.sharedInstance.showAlertMessage(title: "", Message: PHONE_NUMBER_VALIDATION, view: self)
        }
        else if(txtLicenseNumber.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: LICENSENUMBER_REQUIRED_VALIDATION, view: self)
        }
        else if(txtPassword.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: PASSWORD_REQUIRED_VALIDATION, view: self)
        }
        else if((txtPassword.text?.count)! < 6){
            AppData.sharedInstance.showAlertMessage(title: "", Message: PASSWORD_TEXT_VALIDATION, view: self)
        }
        else if(txtConfirmPassword.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: CONFIRMPWD_REQUIRED_VALIDATION, view: self)
        }
        else if(txtConfirmPassword.text != txtPassword.text){
            AppData.sharedInstance.showAlertMessage(title: "", Message: CONFIRMPWD_MATCH_VALIDATION, view: self)
        }
        else if(self.profileImage == nil){
            AppData.sharedInstance.showAlertMessage(title: "", Message: PROFILEIMAGE_REQUIRED_VALIDATION, view: self)
        }
        else if(!AppData.sharedInstance.checkLocationService()){
            let alert = UIAlertController(title: "", message: LOCATION_SERVICE_MESSAGE, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel) { action in
                // perhaps use action.title here
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        })
                    } else {
                        // Fallback on earlier versions
                    }
                }
            })
            self.present(alert, animated: true, completion: nil)
        }
        else if(AppData.sharedInstance.getCurrentLatitude() == nil || AppData.sharedInstance.getCurrentLongitude() == nil){
            AppData.sharedInstance.showAlertMessage(title: "", Message: LOCATION_MESSAGE, view: self)
        }
        else{
            // APi Calling
            signUpUser()
        }
    }
    
    func signUpUser()
    {
        SHOWPROGRESS()
        let c_image = self.imgProfile.image?.resized(withPercentage: 0.6)
        let profileImage = AppData.sharedInstance.convertBase64String(img: c_image!)
        let param: NSDictionary = ["userrole":4,
                                   "firstname":self.txtFName.text!,
                                   "lastname":self.txtLName.text!,
                                   "username":"",
                                   "latitude":AppData.sharedInstance.getCurrentLatitude() ?? "",
                                   "longitude":AppData.sharedInstance.getCurrentLongitude() ?? "",
                                   "address":address,
                                   "email":self.txtEmail.text!,
                                   "license_number":self.txtLicenseNumber.text!,
                                   "password":self.txtPassword.text!,
                                   "phoneno":self.txtPhoneNo.text!,
                                   "devicetype":DEVICE_TYPE,
                                   "device_token":DEVICE_TOKEN,
                                   "profile_image":profileImage,
                                   "access_key":"nousername",
                                   "secret_key":AppData.sharedInstance.getTempToken() ?? ""
        ]
        print(param)
        APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsRegisterV2), parameters: param, completion: { (response, error) in
            if(error == nil){
                self.setResponse(dictResponse: response)
            }
            else{
                HIDEPROGRESS()
            }
        })
    }
    
    func setResponse(dictResponse: NSDictionary?){
        print(dictResponse ?? "")
        HIDEPROGRESS()
        if(dictResponse != nil){
            print(dictResponse!)
            let status = dictResponse?.value(forKey: STATUS) as! String
            if(status == SUCCESS){                
                //                let userToken = dictResponse?.value(forKey: "usertoken") as! String
                //                AppData.sharedInstance.setUserToken(token: userToken)
                //                let userDic = dictResponse?.value(forKey: "user") as! NSDictionary
                //                let userType = userDic.value(forKey: "role_id") as! Int
                //                if(userType == 2){
                //                    AppData.sharedInstance.setUserType(type: 1)
                //                    AppData.sharedInstance.setLoginUser(user: userDic)
                //                    DispatchQueue.main.async {
                ////                        AppData.sharedInstance.appDelegate.setupCustomerUI()
                //                        let orderVC = mainStoryboard.instantiateViewController(withIdentifier: "idHomeNavigation") as! UINavigationController
                //                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                //                        appDelegate.window?.rootViewController = orderVC
                //                        appDelegate.window?.makeKeyAndVisible()
                //                    }
                //                }
                //                else{
                let ApprovalVC = mainStoryboard.instantiateViewController(withIdentifier: "idSignUpApprovalVC") as! SignUpApprovalVC
                self.navigationController?.pushViewController(ApprovalVC, animated: true)
                //                }
                
            }
            else{
                let msg = dictResponse?.value(forKey: "message") as! String
                AppData.sharedInstance.showAlertMessage(title: "", Message: msg, view: self)
            }
        }
        else{
            //AppData.sharedInstance.showAlertMessage(title: "", Message: FAILED_MESSAGE, view: self)
        }
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        if (self.txtEmail.text != "" || self.txtFName.text != "" || self.txtLName.text != "" || self.txtPhoneNo.text != "" || self.txtPassword.text != "" || self.txtConfirmPassword.text != ""){
            
            let alert = UIAlertController(title: "Alert", message:"Details will get lost. Are you sure to exit?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
                // perhaps use action.title here
                self.navigationController?.popViewController(animated: false)
            })
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
                // perhaps use action.title here
            })
            self.present(alert, animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - User Functions
    func setUpTextField(){
        self.txtFName.delegate = self
        self.txtLName.delegate = self
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        var addressString : String = ""
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                else{
                    let pm = placemarks! as [CLPlacemark]
                    
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality!
                        }
                        self.address = addressString
                    }
                }
        })
    }
    
    //MARK:-set Theme method
    func setThemeUI() {
        //set Color
        self.txtEmail.lineColor = ThemeColorGrey
        self.txtFName.lineColor = ThemeColorGrey
        self.txtLName.lineColor = ThemeColorGrey
        self.txtPhoneNo.lineColor = ThemeColorGrey
        self.txtLicenseNumber.lineColor = ThemeColorGrey
        self.txtConfirmPassword.lineColor = ThemeColorGrey
        self.txtPassword.lineColor = ThemeColorGrey
        
        
        self.txtEmail.selectedPlaceHolderColor = ThemeColor
        self.txtFName.selectedPlaceHolderColor = ThemeColor
        self.txtLName.selectedPlaceHolderColor = ThemeColor
        self.txtPassword.selectedPlaceHolderColor = ThemeColor
        self.txtConfirmPassword.selectedPlaceHolderColor = ThemeColor
        self.txtPhoneNo.selectedPlaceHolderColor = ThemeColor
        self.txtLicenseNumber.selectedPlaceHolderColor = ThemeColor

        self.txtEmail.selectedLineColor = ThemeColor
        self.txtFName.selectedLineColor = ThemeColor
        self.txtLName.selectedLineColor = ThemeColor
        self.txtPassword.selectedLineColor = ThemeColor
        self.txtConfirmPassword.selectedLineColor = ThemeColor
        self.txtPhoneNo.selectedLineColor = ThemeColor
        self.txtLicenseNumber.selectedLineColor = ThemeColor
      
        self.txtEmail.textColor = ThemeColorBlack
        self.txtFName.textColor = ThemeColorBlack
        self.txtLName.textColor = ThemeColorBlack
        self.txtPassword.textColor = ThemeColorBlack
        self.txtPhoneNo.textColor = ThemeColorBlack
        self.txtConfirmPassword.textColor = ThemeColorBlack
        self.txtLicenseNumber.textColor = ThemeColorBlack
        
        self.btnSignUp.setTitleColor(ThemeColorWhite, for: .normal)
        
        self.btnLogin.setTitleColor(ThemeColor, for: .normal)
        self.btnLogin.setTitleColor(ThemeColor, for: .highlighted)
        self.btnLogin.setTitleColor(ThemeColor, for: .selected)
        
        //set Fonts
        self.txtPassword.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtFName.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtLName.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtConfirmPassword.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtLicenseNumber.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtEmail.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtPhoneNo.font = UIFont(name: Theme_Font_REGULAR, size: 16)
       
        self.btnSignUp.titleLabel?.font = UIFont(name: Theme_Font_REGULAR, size: 18)
        self.btnLogin.titleLabel?.font = UIFont(name: Theme_Font_REGULAR, size: 14)
    }
}

