//
//  ProfileVC.swift
//  TyfnBoxDriver
//
//  Created by c196 on 18/12/17.
//
    
import UIKit
import HCSStarRatingView

class ProfileVC: UIViewController {
    
    // MARK: - Variables
    var userObj:User!
    // MARK: - IBoutlets
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblContact: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var imgUserProfile: UIImageView!
    @IBOutlet var viewProfileView: UIView!
    @IBOutlet var lblLicenseNumber: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet var viewProfileRate: HCSStarRatingView!
    @IBOutlet var viewProfileRatingHeightConstraint: NSLayoutConstraint!

    // MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        SET_TRACK_EVENT(EVENT: DRIVER_PROFILE_SCREEN)
//        self.viewProfileView.layer.masksToBounds = false
//        self.viewProfileView.layer.borderWidth = 5
//        self.viewProfileView.layer.borderColor = UIColor.red.cgColor
//        self.viewProfileView.layer.shadowColor = UIColor.black.cgColor
//        self.viewProfileView.layer.shadowOpacity = 0.3
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setThemeUI()
        self.userObj = User.init(dictionary: AppData.sharedInstance.getLoginUser()!)
        self.setUpData()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnOnlyVisibleYouClick(_ sender: Any) {
        
    }
    
    @IBAction func btnEditProfileClick(_ sender: Any) {
        let editProfileVC = mainStoryboard.instantiateViewController(withIdentifier: "idEditProfileVC") as! EditProfileVC
        self.navigationController?.pushViewController(editProfileVC, animated: true)
    }

    @IBAction func btnSettingClicked(_ sender: UIButton) {
        let editProfileVC = settingStoryboard.instantiateViewController(withIdentifier: "idSettingVC") as! SettingVC
        self.navigationController?.pushViewController(editProfileVC, animated: true)
    }
    
    // MARK: - User Functions
    func setUpData(){
        AppData.sharedInstance.roundAvatarImage(imageview: self.imgUserProfile)
        AppData.sharedInstance.setViewBorder(view: self.viewProfileView, color: ThemeColorGrey)
        if(userObj != nil){
            self.lblUserName.text = ((userObj.first_name ?? "")+" "+(userObj.last_name ?? ""))
            self.lblEmail.text = userObj.email ?? PLACEHOLDER_EMAIL
            if(userObj.phone_number !=  "" ){
                self.lblContact.text = String(describing: userObj.phone_number!)
            }
            else{
                self.lblContact.text = PLACEHOLDER_CONTACT
            }
            if(userObj.home_address !=  "" ){
                self.lblLocation.text = userObj.home_address
            }else{
                self.lblLocation.text = PLACEHOLDER_LOCATION
            }
            SET_IMAGEURL(IMAGEVIEW: self.imgUserProfile, IMAGE: PROFILE_IMAGE_URL+(userObj.profile_image ?? ""), PLACEHOLDER: #imageLiteral(resourceName: "Profile_placeholder"))
            self.lblLicenseNumber.text =  userObj.licenseNumber ?? ""
        }
    }
    //MARK:-set Theme method
    func setThemeUI() {
        //set Color

        self.lblUserName.textColor = ThemeColorBlack
        self.lblEmail.textColor = ThemeColorBlack
        self.lblContact.textColor = ThemeColorBlack
        self.lblLocation.textColor = ThemeColorBlack
        self.lblLicenseNumber.textColor = ThemeColorBlack
        
        self.btnEdit.setTitleColor(ThemeColorWhite, for: .normal)
        
        //set Fonts
        self.lblUserName.font = UIFont(name: Theme_Font_REGULAR, size: 14)
        self.lblEmail.font = UIFont(name: Theme_Font_REGULAR, size: 14)
        self.lblContact.font = UIFont(name: Theme_Font_REGULAR, size: 14)
        self.lblLocation.font = UIFont(name: Theme_Font_REGULAR, size: 14)
        self.lblLicenseNumber.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        
        self.btnEdit.titleLabel?.font = UIFont(name: Theme_Font_REGULAR, size: 18)
    }
}

