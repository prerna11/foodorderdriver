//
//  EditProfileVC.swift
//  TyfnBoxDriver
//
//  Created by c196 on 18/12/17.
//

import UIKit
import ACFloatingTextfield_Objc
import GooglePlaces

class EditProfileVC: UIViewController,SelectImageDelegate,UITextFieldDelegate,GMSAutocompleteViewControllerDelegate {
    
    // MARK: - Variables
    var userObj:User!
    var profileImage:UIImage!
    var lat:String?
    var long:String?
    
    // MARK: - IBoutlets
    @IBOutlet var txtFirstName: ACFloatingTextField!
    @IBOutlet var txtLastName: ACFloatingTextField!
    @IBOutlet var txtEmail: ACFloatingTextField!
    @IBOutlet var txtContact: ACFloatingTextField!
    @IBOutlet var txtLicenseNumber: ACFloatingTextField!
    @IBOutlet var txtLocation: ACFloatingTextField!
    @IBOutlet var imgUserProfile: UIImageView!
    @IBOutlet var viewAboutUser: UIView!
    @IBOutlet var txtAboutYou: ACFloatingTextField!
    @IBOutlet weak var btnsave: UIButton!
    @IBOutlet var constratintViewAboutHeight: NSLayoutConstraint!
    
    // MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        SET_TRACK_EVENT(EVENT: EDIT_PROFILE_SCREEN)
        self.setUpTextField()
        self.userObj = User.init(dictionary: AppData.sharedInstance.getLoginUser()!)
        self.setUpData()
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setThemeUI()
        self.userObj = User.init(dictionary: AppData.sharedInstance.getLoginUser()!)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Google Place Textfield Delegate / Datasource
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        self.lat = String(place.coordinate.latitude)
        self.long = String(place.coordinate.longitude)
        self.txtLocation.text = String(describing: place.formattedAddress!)
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    // MARK: - Delegate / Datasource
    func textFieldDidEndEditing(_ textField: UITextField) {
        let str = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if(str == nil || str == ""){
            textField.text = ""
        }
    }
    
    // MARK: - Select Image Delegate
    func didFinishSelectImage(image: UIImage?, videoURL: NSURL?) {
        self.dismiss(animated: false, completion: nil)
        if(image != nil){
            self.profileImage = image
            self.imgUserProfile.image = image
        }
    }
    
    // MARK: - Button Actions
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUserProfileClick(_ sender: UIButton) {
        let SelectImagePopup = mainStoryboard.instantiateViewController(withIdentifier: "idTBPickImageDelegateVC") as! TBPickImageDelegateVC
        SelectImagePopup.modalPresentationStyle = .overFullScreen
        SelectImagePopup.delegate = self
        SelectImagePopup.view_title = "Select Profile Photo"
        SelectImagePopup.fromProfile = true
        SelectImagePopup.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        SelectImagePopup.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            SelectImagePopup.view.alpha = 1.0
            SelectImagePopup.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
        self.present(SelectImagePopup, animated: false, completion: nil)
    }
    
    @IBAction func btnLocationClicked(_ sender: UIButton) {
        openGooglePlaceTextField()
    }
    
    func openGooglePlaceTextField(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func btnSaveClick(_ sender: Any) {
        self.view.endEditing(true)
        if(self.txtFirstName.text == "")
        {
            AppData.sharedInstance.showAlertMessage(title: "", Message: FNAME_REQUIRED_VALIDATION, view: self)
        }
        else if(self.txtLastName.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: LNAME_REQUIRED_VALIDATION, view: self)
        }
        else if(txtContact.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: PHONE_REQUIRED_VALIDATION, view: self)
        }
        else if(txtContact.text != "" && ((txtContact.text?.count)! < 6 || (txtContact.text?.count)! > 15)){
            AppData.sharedInstance.showAlertMessage(title: "", Message: PHONE_NUMBER_VALIDATION, view: self)
        }
        else if(self.txtLicenseNumber.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: LICENSENUMBER_REQUIRED_VALIDATION, view: self)
        }
        else{
            self.EditProfileAPI()
        }
    }
    
    // MARK: - User Functions
    func setUpTextField(){
        self.txtFirstName.delegate = self
        self.txtLastName.delegate = self
    }
    func setUpData(){
        self.txtFirstName.text = userObj.first_name ?? ""
        self.txtLastName.text = userObj.last_name ?? ""
        self.txtEmail.text = userObj.email ?? ""
        self.txtContact.text = userObj.phone_number ?? ""
        self.txtLocation.text = userObj.home_address ?? ""
        self.txtLicenseNumber.text = userObj.licenseNumber
        //            self.txtAboutYou.text = userObj.user_desc ?? ""
        AppData.sharedInstance.roundAvatarImage(imageview: self.imgUserProfile)
        SET_IMAGEURL(IMAGEVIEW: self.imgUserProfile, IMAGE: PROFILE_IMAGE_URL+(userObj.profile_image ?? ""), PLACEHOLDER: #imageLiteral(resourceName: "Profile_placeholder"))
    }
    
    func EditProfileAPI(){
        SHOWPROGRESS()
        var profileImageStr = ""
        if(self.profileImage != nil){
            let c_image = self.imgUserProfile.image?.resized(withPercentage: 0.6)
            profileImageStr = AppData.sharedInstance.convertBase64String(img: c_image!)
        }
        let param: NSDictionary = [    "userId":userObj.id ?? 0,
                                       "firstname":txtFirstName.text ?? "",
                                       "lastname":txtLastName.text ?? "",
                                       "phoneno":txtContact.text ?? "",
                                       "license_number":self.txtLicenseNumber.text!,
                                       "profile_image":profileImageStr,
                                       "location":txtLocation.text ?? ""
        ]
        APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsEditProfileV2), parameters: param, completion: { (response, error) in
            if(error == nil){
                self.EditProfileResponse(dictResponse: response)
            }
            else{
                HIDEPROGRESS()
            }
        })
    }
    func EditProfileResponse(dictResponse: NSDictionary?){
        print(dictResponse ?? "")
        HIDEPROGRESS()
        if(dictResponse != nil){
            let status = dictResponse?.value(forKey: STATUS) as! String
            if(status == SUCCESS){
                let is_active = dictResponse?.value(forKey: USER_ACTIVE) as! Int
                if(is_active == 0){
                    AppData.sharedInstance.MANAGE_DEACTIVE_ACCOUNT_FLOW(controller: self, message: DEACTIVE_ACCOUNT_MESSAGE)
                }
                else{
                    let userDic = dictResponse?.value(forKey: "user") as! NSDictionary
                    AppData.sharedInstance.setLoginUser(user: userDic)
                    let alert = UIAlertController(title: "", message: PROFILE_UPDATED_MESSAGE, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
                        // perhaps use action.title here
                        self.navigationController?.popViewController(animated: true)
                    })
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                AppData.sharedInstance.showAlertMessage(title: "", Message: FAILED_MESSAGE, view: self)
            }
        }
    }
    // MARK: - Navigation
    
    
    //MARK:-set Theme method
    func setThemeUI() {
        //set Color
        self.txtEmail.lineColor = ThemeColorGrey
        self.txtFirstName.lineColor = ThemeColorGrey
        self.txtLastName.lineColor = ThemeColorGrey
        self.txtLocation.lineColor = ThemeColorGrey
        self.txtLicenseNumber.lineColor = ThemeColorGrey
        self.txtContact.lineColor = ThemeColorGrey
        self.txtAboutYou.lineColor = ThemeColorGrey
        
        
        self.txtEmail.selectedPlaceHolderColor = ThemeColor
        self.txtFirstName.selectedPlaceHolderColor = ThemeColor
        self.txtLastName.selectedPlaceHolderColor = ThemeColor
        self.txtLocation.selectedPlaceHolderColor = ThemeColor
        self.txtLicenseNumber.selectedPlaceHolderColor = ThemeColor
        self.txtContact.selectedPlaceHolderColor = ThemeColor
        self.txtAboutYou.selectedPlaceHolderColor = ThemeColor
        
        
        self.txtEmail.selectedLineColor = ThemeColor
        self.txtFirstName.selectedLineColor = ThemeColor
        self.txtLastName.selectedLineColor = ThemeColor
        self.txtLocation.selectedLineColor = ThemeColor
        self.txtLicenseNumber.selectedLineColor = ThemeColor
        self.txtContact.selectedLineColor = ThemeColor
        self.txtAboutYou.selectedLineColor = ThemeColor
        
        self.txtEmail.textColor = ThemeColorBlack
        self.txtFirstName.textColor = ThemeColorBlack
        self.txtLastName.textColor = ThemeColorBlack
        self.txtLocation.textColor = ThemeColorBlack
        self.txtLicenseNumber.textColor = ThemeColorBlack
        self.txtContact.textColor = ThemeColorBlack
        self.txtAboutYou.textColor = ThemeColorBlack
        
        self.btnsave.setTitleColor(ThemeColorWhite, for: .normal)
        
        //set Fonts
        self.txtFirstName.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtLastName.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtLocation.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtContact.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtLicenseNumber.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtEmail.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtAboutYou.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        
        self.btnsave.titleLabel?.font = UIFont(name: Theme_Font_REGULAR, size: 18)
    }
}

