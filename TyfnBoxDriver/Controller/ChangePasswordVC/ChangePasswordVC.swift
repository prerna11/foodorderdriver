//
//  ChangePasswordVC.swift
//  TyfnBoxDriver
//
//  Created by c196 on 21/12/17.
//

import UIKit
import ACFloatingTextfield_Objc

class ChangePasswordVC: UIViewController {

    // MARK: - Variables
    var userObj:User!
    // MARK: - IBoutlets
    @IBOutlet var txtOldPassword: ACFloatingTextField!
    @IBOutlet var txtNewPassword: ACFloatingTextField!
    @IBOutlet var txtConfirmPassword: ACFloatingTextField!
    @IBOutlet weak var btnChangePass: UIButton!
    
    // MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        SET_TRACK_EVENT(EVENT: CHANGE_PASSWORD_SCREEN)
        self.title = "Change Password"
        self.setNavigationMenu()
        self.userObj = User.init(dictionary: AppData.sharedInstance.getLoginUser()!)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setThemeUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Deleagte/Datasources
    // MARK: - Button Actions
    @IBAction func btnChangePasswordClick(_ sender: UIButton) {
        if(txtOldPassword.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: OLD_PASSWORD_REQUIRED_VALIDATION, view: self)
        }
        else if(txtNewPassword.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: NEW_PASSWORD_REQUIRED_VALIDATION, view: self)
        }
        else if((txtNewPassword.text?.count)! < 6){
            AppData.sharedInstance.showAlertMessage(title: "", Message: PASSWORD_TEXT_VALIDATION, view: self)
        }
        else if(txtOldPassword.text ==  txtNewPassword.text){
            AppData.sharedInstance.showAlertMessage(title: "", Message: OLD_NEW_PASSWORD_VALIDATION, view: self)
        }
        else if(txtConfirmPassword.text == ""){
            AppData.sharedInstance.showAlertMessage(title: "", Message: CONFIRMPWD_REQUIRED_VALIDATION, view: self)
        }
        else if(txtNewPassword.text !=  txtConfirmPassword.text){
            AppData.sharedInstance.showAlertMessage(title: "", Message: NEWPWD_MATCH_VALIDATION, view: self)
        }
        else{
            self.changePasswordAPI()
        }
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    // MARK: - User Functions
    func changePasswordAPI(){
        SHOWPROGRESS()
        let param: NSDictionary = ["userId":userObj.id ?? 0,
                                   "oldpassword":txtOldPassword.text ?? "",
                                   "newpassword":txtNewPassword.text ?? ""]
        APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsChangePassword), parameters: param) { (response, error) in
            if(error == nil){
                self.changePasswordResponse(dict: response)
            }
            else{
                HIDEPROGRESS()
            }
        }
    }
    
    func changePasswordResponse(dict:NSDictionary?){
        HIDEPROGRESS()
        if(dict != nil){
            let status = dict?.value(forKey: STATUS) as! String
            if(status == SUCCESS){
                let is_active = dict?.value(forKey: USER_ACTIVE) as! Int
                if(is_active == 0){
                    AppData.sharedInstance.MANAGE_DEACTIVE_ACCOUNT_FLOW(controller: self, message: DEACTIVE_ACCOUNT_MESSAGE)
                }
                else{
                let alert = UIAlertController(title: "", message: dict?.value(forKey: "message") as? String, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel) { action in
                    // perhaps use action.title here
                    
//                    self.navigationController?.popViewController(animated: false)
                    

                    AppData.sharedInstance.logoutAPI(controller: self)
                    UserDefaults.standard.removeObject(forKey: "userType")
                    DispatchQueue.main.async {
                        AppData.sharedInstance.appDelegate.setupLoginUI()
                    }
                    
                })
                self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                AppData.sharedInstance.showAlertMessage(title: "", Message: dict?.value(forKey: "message") as! String, view: self)
            }
        }
    }
    
    // MARK: - Navigation
    func setNavigationMenu(){
        let left = UIBarButtonItem(image: UIImage(named: "ic_BackBlue"), style: .plain, target: self, action: #selector(self.leftClick)) // action:#selector(Class.MethodName) for swift 3
        left.tintColor = ThemeColor
        self.navigationItem.leftBarButtonItem  = left
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:ThemeColor]
    }
    
    @objc func leftClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-set Theme method
    func setThemeUI() {
        //set Color
        self.txtNewPassword.lineColor = ThemeColorGrey
        self.txtOldPassword.lineColor = ThemeColorGrey
        self.txtConfirmPassword.lineColor = ThemeColorGrey

        self.txtOldPassword.selectedPlaceHolderColor = ThemeColor
        self.txtNewPassword.selectedPlaceHolderColor = ThemeColor
        self.txtConfirmPassword.selectedPlaceHolderColor = ThemeColor
        
        self.txtOldPassword.selectedLineColor = ThemeColor
        self.txtNewPassword.selectedLineColor = ThemeColor
        self.txtConfirmPassword.selectedLineColor = ThemeColor
        
        self.txtNewPassword.textColor = ThemeColorBlack
        self.txtOldPassword.textColor = ThemeColorBlack
        self.txtConfirmPassword.textColor = ThemeColorBlack

        self.btnChangePass.setTitleColor(ThemeColorWhite, for: .normal)
        
        //set Fonts
        self.txtNewPassword.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtOldPassword.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        self.txtConfirmPassword.font = UIFont(name: Theme_Font_REGULAR, size: 16)
        
        self.btnChangePass.titleLabel?.font = UIFont(name: Theme_Font_REGULAR, size: 18)
    }
}

