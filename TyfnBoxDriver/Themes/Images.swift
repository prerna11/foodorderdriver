//
//  Images.swift
//  NafasDemo
//
//  Created by NC2-41 on 05/09/18.
//  Copyright © 2018 NC2-41. All rights reserved.
//

import Foundation

public var IMG_icon_main : String = ""

public var IMG_LOGO : String = ""
public var IMG_ICON_AcceptColor: String = ""
public var IMG_ICON_RejectGrey = ""
public var IMG_BG_RED = ""
public var IMG_ICON_SIDE_ARROW = ""
public var IMG_ICON_Divider: String = ""
public var IMG_ICON_UserLocation : String = ""
public var IMG_ICON_Chef_Message : String = ""
public var IMG_shadow : String = ""
public var IMG_BACK_WHITE: String = ""
public var IMG_Camera: String = ""
public var IMG_Gallary: String = ""
public var IMG_ICON_PlusSign: String = ""
public var IMG_ICON_NAV_LOGO: String = ""

public var IMG_ICON_BtnAcceptColor : String = ""
public var IMG_ICON_BtnRejectGrey : String = ""
public var IMG_ICON_PickedUp : String = ""
public var IMG_ICON_Delivered : String = ""
public var IMG_ICON_Accepted : String = ""
public var IMG_ICON_downArrow : String = ""

public var IMG_BACK_BLACK : String = ""
public var IMG_NO_IMG_Thumb : String = ""
public var IMG_ICON_PH_USER_TN : String = ""
public var IMG_ICON_PH_USER : String = ""

public func setImages(type:Theme)
{
    switch type
    {
    case .app1:
        
        
        //MARK:- Images Frequently Used
        IMG_icon_main = "icon_main"
        IMG_LOGO = "Logo"
        IMG_BACK_BLACK = "Back_black"
        IMG_ICON_AcceptColor = "btnColorBG"
        IMG_ICON_RejectGrey = "btnGreyBG"
        IMG_NO_IMG_Thumb = "NoImage_Thumb"
        IMG_ICON_PH_USER_TN = "profile_placeholder_thumb"
        IMG_ICON_PH_USER = "ic_User"
        IMG_BG_RED =  "Profile_Background"
        
        IMG_ICON_BtnAcceptColor = "btnAcceptColor"
        IMG_ICON_BtnRejectGrey = "btnDeclineGrey"
   
        
        IMG_ICON_PickedUp = "btnPickedUp"
        IMG_ICON_Delivered = "btnDelivered"
        IMG_ICON_Accepted = "btnAccepted"
        
        IMG_ICON_UserLocation = "Location"
        IMG_ICON_Chef_Message = "Message"
        IMG_shadow = "Customer_home_shadow"
        IMG_BACK_WHITE = "back_white"
        IMG_Camera = "Camera_Unselect"
        IMG_Gallary = "Gallery_Unselect"
        IMG_ICON_PlusSign = "Add"
        IMG_ICON_NAV_LOGO = "navImage"
        break;
        
    case .app2:
        
        //MARK:- Images Frequently Used
        IMG_icon_main = "icon_main"
        IMG_LOGO = "Logo"
        IMG_BACK_BLACK = "Back_black"
        IMG_ICON_AcceptColor = "btnColorBG"
        IMG_ICON_RejectGrey = "btnGreyBG"
        IMG_NO_IMG_Thumb = "NoImage_Thumb"
        IMG_ICON_PH_USER_TN = "profile_placeholder_thumb"
        IMG_ICON_PH_USER = "ic_User"
        IMG_BG_RED =  "Profile_Background"
        IMG_ICON_SIDE_ARROW = "SideArrow"
        IMG_ICON_BtnAcceptColor = "btnAcceptColor"
        IMG_ICON_BtnRejectGrey = "btnDeclineGrey"
        
        IMG_ICON_PickedUp = "btnPickedUp"
        IMG_ICON_Delivered = "btnDelivered"
        IMG_ICON_Accepted = "btnAccepted"
        
        IMG_ICON_UserLocation = "Location"
        IMG_ICON_Chef_Message = "Message"
        IMG_shadow = "Customer_home_shadow"
        IMG_BACK_WHITE = "back_white"
        IMG_Camera = "Camera_Unselect"
        IMG_Gallary = "Gallery_Unselect"
        IMG_ICON_NAV_LOGO = "navImage"
        break;
        
    case .app3:
        IMG_icon_main = "icon_main"
        IMG_LOGO = "Logo"
        IMG_BACK_BLACK = "Back_black"
        IMG_ICON_AcceptColor = "btnColorBG"
        IMG_ICON_RejectGrey = "btnGreyBG"
        IMG_NO_IMG_Thumb = "NoImage_Thumb"
        IMG_ICON_PH_USER_TN = "profile_placeholder_thumb"
        IMG_ICON_PH_USER = "ic_User"
        IMG_BG_RED =  "Profile_Background"
        IMG_ICON_SIDE_ARROW = "SideArrow"
        IMG_ICON_BtnAcceptColor = "btnAcceptColor"
        IMG_ICON_BtnRejectGrey = "btnDeclineGrey"
        IMG_ICON_downArrow = "Down_Arrow"
        IMG_ICON_Divider = "Table_Divider"
        IMG_Camera = "Camera_Unselect"
        IMG_Gallary = "Gallery_Unselect"
        
        IMG_ICON_PickedUp = "btnPickedUp"
        IMG_ICON_Delivered = "btnDelivered"
        IMG_ICON_Accepted = "btnAccepted"
        
        IMG_ICON_UserLocation = "Location"
        IMG_ICON_Chef_Message = "Message"
        IMG_ICON_NAV_LOGO = "navImage"
        break ;
        
    case .app4:
        IMG_icon_main = "icon_main"
        IMG_LOGO = "Logo"
        IMG_BACK_BLACK = "Back_black"
        IMG_ICON_AcceptColor = "btnColorBG"
        IMG_ICON_RejectGrey = "btnGreyBG"
        IMG_NO_IMG_Thumb = "NoImage_Thumb"
        IMG_ICON_PH_USER_TN = "profile_placeholder_thumb"
        IMG_ICON_PH_USER = "ic_User"
        IMG_BG_RED =  "Profile_Background"
        IMG_ICON_SIDE_ARROW = "SideArrow"
        IMG_ICON_BtnAcceptColor = "btnAcceptColor"
        IMG_ICON_BtnRejectGrey = "btnDeclineGrey"
        IMG_ICON_downArrow = "Down_Arrow"
        IMG_ICON_Divider = "Table_Divider"
        IMG_Camera = "Camera_Unselect"
        IMG_Gallary = "Gallery_Unselect"
        
        IMG_ICON_PickedUp = "btnPickedUp"
        IMG_ICON_Delivered = "btnDelivered"
        IMG_ICON_Accepted = "btnAccepted"
        
        IMG_ICON_UserLocation = "Location"
        IMG_ICON_Chef_Message = "Message"
        IMG_ICON_NAV_LOGO = "navImage"
        break ;
    }
}

