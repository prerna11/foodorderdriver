//
//  Fonts.swift
//  NafasDemo
//
//  Created by NC2-41 on 05/09/18.
//  Copyright © 2018 NC2-41. All rights reserved.
//

import Foundation
import UIKit

    public var Theme_Font_REGULAR : String = ""
    public var Theme_Font_OBLIQUE : String = ""
    public var Theme_Font_BOLD_OBLIQUE : String = ""
    public var Theme_Font_LIGHT : String = ""
    public var Theme_Font_ULTRALIGHT : String = ""
    public var Theme_Font_BOLD : String = ""

    public func setFonts(type:Theme)
    {
        switch type
        {
        case .app1:
     
            Theme_Font_REGULAR = "ArialMT"
            Theme_Font_OBLIQUE = "Arial-ItalicMT"
            Theme_Font_BOLD_OBLIQUE = "Arial-BoldItalicMT"
            //Theme_Font_LIGHT = "Helvetica-Light"
            //Theme_Font_ULTRALIGHT = "Helvetica-LightOblique"
            Theme_Font_BOLD = "Arial-BoldMT"
            break
        case .app2:
            Theme_Font_REGULAR = "ArialMT"
            Theme_Font_OBLIQUE = "Arial-ItalicMT"
            Theme_Font_BOLD_OBLIQUE = "Arial-BoldItalicMT"
            //Theme_Font_LIGHT = "Helvetica-Light"
            //Theme_Font_ULTRALIGHT = "Helvetica-LightOblique"
            Theme_Font_BOLD = "Arial-BoldMT"
            break
        case .app3:
            Theme_Font_REGULAR = "Helvetica3"
            Theme_Font_OBLIQUE = "Helvetica-Oblique3"
            Theme_Font_BOLD_OBLIQUE = "Helvetica-BoldOblique3"
            Theme_Font_LIGHT = "Helvetica-Light3"
            Theme_Font_ULTRALIGHT = "Helvetica-LightOblique3"
            Theme_Font_BOLD = "Helvetica-Bold3"
            break
        case .app4:
            Theme_Font_REGULAR = "ArialMT"
            Theme_Font_OBLIQUE = "Arial-ItalicMT"
            Theme_Font_BOLD_OBLIQUE = "Arial-BoldItalicMT"
            //Theme_Font_LIGHT = "Helvetica-Light"
            //Theme_Font_ULTRALIGHT = "Helvetica-LightOblique"
            Theme_Font_BOLD = "Arial-BoldMT"
            break
        }
    }

    














