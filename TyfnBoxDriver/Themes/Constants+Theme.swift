//
//  Constants+Theme.swift
//  TyfnBox
//
//  Created by NC2-41 on 06/09/18.
//  Copyright © 2018 C100-11. All rights reserved.
//

import Foundation

public enum Theme : String
{
    case app1
    case app2
    case app3
    case app4
}

public func setThemes(type:Theme)
{
    switch type
    {
    case .app1:
        setFonts(type: type)
        setImages(type: type)
        setColor(type: type)
        break;
    case .app2:
        setFonts(type: type)
        setImages(type: type)
        setColor(type: type)
    case .app3:
        setFonts(type: type)
        setImages(type: type)
        setColor(type: type)
    case .app4:
        setFonts(type: type)
        setImages(type: type)
        setColor(type: type)
    }
}
