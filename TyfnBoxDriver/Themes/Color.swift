//
//  Color.swift
//  NafasDemo
//
//  Created by NC2-41 on 05/09/18.
//  Copyright © 2018 NC2-41. All rights reserved.
//

import Foundation
import UIKit

public var ThemeColor = UIColor.clear
public var ThemeColorGrey = UIColor.clear
public var ThemeColorDarkGrey = UIColor.clear
public var ThemeColorOrange = UIColor.clear
public var ThemeColorDullGreen = UIColor.clear
public var ThemeColorBlack = UIColor.clear
public var ThemeColorWhite = UIColor.clear

public var ThemeColorRed = UIColor.clear
public var ThemeColorRed1 = UIColor.clear


public func setColor(type:Theme)
{
    switch type
    {
    case .app1:
        if #available(iOS 10.0, *) {
            ThemeColor = UIColor(red: 255.0/255.0, green: 102.0/255.0, blue: 43.0/255.0, alpha: 1.0)
            ThemeColorGrey = UIColor(red: 163.0/255.0, green: 163.0/255.0, blue: 163.0/255.0, alpha: 1.0)
            ThemeColorDarkGrey = UIColor(red: 236.0/255.0, green: 240.0/255.0, blue: 245.0/255.0, alpha: 1.0)
            ThemeColorWhite = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            ThemeColorBlack = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            
        } else {
            // Fallback on earlier versions
        }
       
        break
    case .app2:
        if #available(iOS 10.0, *) {
            ThemeColor =  UIColor(red: 79.0/255.0, green: 155.0/255.0, blue: 247.0/255.0, alpha: 1.0)
            ThemeColorGrey = UIColor(red: 163.0/255.0, green: 163.0/255.0, blue: 163.0/255.0, alpha: 1.0)
            ThemeColorDarkGrey = UIColor(red: 236.0/255.0, green: 240.0/255.0, blue: 245.0/255.0, alpha: 1.0)
            ThemeColorWhite = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            ThemeColorBlack = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            
        } else {
            // Fallback on earlier versions
        }
       
        break
    case .app3:
        if #available(iOS 10.0, *) {
            ThemeColor = UIColor(red: 255.0/255.0, green: 102.0/255.0, blue: 43.0/255.0, alpha: 1.0)
            ThemeColorGrey = UIColor(red: 163.0/255.0, green: 163.0/255.0, blue: 163.0/255.0, alpha: 1.0)
            ThemeColorDarkGrey = UIColor(red: 236.0/255.0, green: 240.0/255.0, blue: 245.0/255.0, alpha: 1.0)
            ThemeColorWhite = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            ThemeColorBlack = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            
        } else {
            // Fallback on earlier versions
        }
       
        break
        
    case .app4:
        if #available(iOS 10.0, *)
        {
            ThemeColorRed = UIColor(displayP3Red: 0/255.0, green: 190/255.0, blue: 78/255.0, alpha: 1.0)
            ThemeColorGrey = UIColor(displayP3Red: 163.0/255.0, green: 163.0/255.0, blue: 163.0/255.0, alpha: 1.0)
            ThemeColorDarkGrey = UIColor(displayP3Red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
            ThemeColorOrange = UIColor(displayP3Red: 248/255.0, green: 91/255.0, blue: 74/255.0, alpha: 1.0)
            ThemeColorDullGreen = UIColor(displayP3Red: 112/255.0, green: 173/255.0, blue: 71/255.0, alpha: 1.0)
            
            
            ThemeColor = UIColor(displayP3Red: 0/255.0, green: 190/255.0, blue: 78/255.0, alpha: 1.0)
//            UIColor(red: 255.0/255.0, green: 102.0/255.0, blue: 43.0/255.0, alpha: 1.0)
//            ThemeColorGrey = UIColor(red: 163.0/255.0, green: 163.0/255.0, blue: 163.0/255.0, alpha: 1.0)
//            ThemeColorDarkGrey = UIColor(red: 236.0/255.0, green: 240.0/255.0, blue: 245.0/255.0, alpha: 1.0)
            ThemeColorWhite = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            ThemeColorBlack = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        }
        else
        {
            // Fallback on earlier versions
        }
        
        break
    }
}
