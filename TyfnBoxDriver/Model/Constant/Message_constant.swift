//
//  Message_constant.swift
//  TyfnBox
//
//  Created by C100-11 on 01/12/17.
//  Copyright © 2017 C100-11. All rights reserved.
//

import Foundation

let FAILED_MESSAGE = "Something went wrong."
let NOORDER_MESSAGE = "No orders currently!"
let DEACTIVE_ACCOUNT_MESSAGE = "Your account is deactivated. Please contact to TyfnBox support team.";

let LOCATION_MESSAGE = "Please check your location service once.";

let FNAME_REQUIRED_VALIDATION = "Please enter first name.";
let LNAME_REQUIRED_VALIDATION = "Please enter last name.";
let EMAIL_REQUIRED_VALIDATION = "Please enter email address.";
let EMAIL_TEXT_VALIDATION = "Please enter valid email address.";
let PHONE_REQUIRED_VALIDATION = "Please enter phone number.";
let PASSWORD_REQUIRED_VALIDATION = "Please enter password.";
let PASSWORD_TEXT_VALIDATION = "Password must be at least 6 characters.";
let PHONE_NUMBER_VALIDATION = "Phone number should be at least 6-15 digits.";
let LICENSENUMBER_REQUIRED_VALIDATION = "Please enter License Number.";
let CONFIRMPWD_REQUIRED_VALIDATION = "Please enter confirm password.";
let CONFIRMPWD_MATCH_VALIDATION = "Password and confirm password not match.";
let PROFILEIMAGE_REQUIRED_VALIDATION = "Please select your profile picture.";
let VIDEO_LIMIT_VALIDATION = "Please Choose your video of 1 minute length.";
let VIDEO_EXIST_VALIDATION = "You have already choose one video.";
let IMAGE_LIMIT_VALIDATION = "You have already choose 5 images.";
let PROFILE_UPDATED_MESSAGE = "Profile updated successfully.";
let DISH_IMAGE_VALIDATION = "You have upload at least one dish image.";
let DISH_NAME_REQUIRED_VALIDATION = "Please enter dish name.";
let DISH_PRICE_REQUIRED_VALIDATION = "Please enter dish Price.";
let DISH_PRICE_VALIDATION = "Dish price must be greater than 0.";
let DISH_QTY_REQUIRED_VALIDATION = "Please enter dish quantity.";
let DISH_QTY_VALIDATION = "Dish Quantity must be greater than 0.";
let DISH_QTY_LENGTH_VALIDATION = "Dish Quantity length maximum 3 digits.";
let DISH_DESC_REQUIRED_VALIDATION = "Please enter dish description.";
let DISH_EXPIRY_REQUIRED_VALIDATION = "Please select dish expiry date.";
let OLD_PASSWORD_REQUIRED_VALIDATION = "Please enter old password.";
let NEW_PASSWORD_REQUIRED_VALIDATION = "Please enter new password.";
let NEWPWD_MATCH_VALIDATION = "New and Confirm password did not match.";
let OLD_NEW_PASSWORD_VALIDATION = "Old and New Password must be different.";
let LOGOUT_CONFIRMATION_MESSAGE = "Are you sure want to logout?";
let ADDRESS_TYPE_REQUIRED_VALIDATION = "Please Select Address Type.";
let ADDRESS_REQUIRED_VALIDATION = "Please enter address.";
let ADDRESS_POSTAL_CODE_REQUIRED_VALIDATION = "Please enter postal code.";
let ADDRESS_CITY_REQUIRED_VALIDATION = "Please enter city.";
let ADDRESS_COUNTRY_REQUIRED_VALIDATION = "Please select country.";
let SELECT_CARD_VALIDATION = "Please select card for payment.";
let DISH_AVAILABLE_QTY_VALIDATION = "Dish Available quantity is less. Please make sure your order quantity.";
let DISH_INACTIVE_INFO_MESSAGE = "Your dish will be invisible state. Customer can not see your dish in list.";
let DELIVERY_ADDRESS_VALIDATION = "Please select delivery address.";
let CARD_HOLDER_NAME_REQUIRED_VALIDATION = "Please enter card holder name.";
let CARD_NUMBER_REQUIRED_VALIDATION = "Please enter card number.";
let CARD_EXPIRY_MONTH_REQUIRED_VALIDAITION = "Please enter card expiry month.";
let CARD_EXPIRY_YEAR_REQUIRED_VALIDAITION = "Please enter card expiry year.";
let CARD_CVV_REQUIRED_VALIDATION = "Please enter card cvv.";


let ABOUT_US_WEB_URL_MESSAGE = "Please check after some time currently this page is under construction."
let TERMS_AND_CONDITION_URL_MESSAGE = "Please check after some time currently this page is under construction."


let Decline_CONFIRMATION_MESSAGE = "Are you sure want to decline this order?";
let STRIPE_ACCOUNT_EXIST_MESSAGE = "You have already submitted your payment details."
let LOCATION_SERVICE_MESSAGE = "Your location service is disable. Please make sure enable location service for continue process.";
let STRIPE_ACCOUNT_NOT_EXIST_MESSAGE = "Your Account is not linked with bank account. Please add first your payment details."


//MARK: - Track event string
let APP_LAUNCH = "APPLICATION LAUNCH"
let WELCOME_SCREEN = "Welcome screen"
let SIGNUP_SCREEN = "SIGNUP SCREEN"
let DRIVER_ORDER_SCREEN = "DRIVER ORDER SCREEN"
let DRIVER_ORDER_DETAIL_SCREEN = "DRIVER ORDER DETAIL SCREEN"
let CUSTOMER_ORDER_SCREEN = "CUSTOMER ORDER SCREEN"
let APPROVAL_SCREEN = "Signup Approval screen"
let LOGIN_SCREEN = "Login screen"
let FORGOT_PASSWORD_SCREEN = "Forgot password screen"
let SELECT_USERTYPE_SCREEN = "Select User Type screen"
let CUSTOMER_HOME_SCREEN = "Customer Home screen"
let DISH_DETAIL_SCREEN = "Dish Detail screen"
let GOOGLE_PLACE_SCREEN = "Search location with google place API"
let CHAT_SCREEN = "Chat screen"
let MESSAGE_SEND_EVENT = "Message Sent"
let ADD_DISH_SCREEN = "Add Dish Screen"
let EDIT_DISH_SCREEN = "Edit Dish Screen"
let SAVE_DISH = "Save New Dish"
let EDIT_DISH = "Edit Dish"
let DISH_STATUS_SCREEN = "Dish Status Screen"
let CHEF_DISH_LIST_SCREEN = "Chef Dish List Screen"
let DELIVERY_ADDRESS_SCREEN = "Delivery Address Screen"
let ADD_DELIVERY_ADDRESS_SCREEN = "Add Delivery Address Screen"
let SEARCH_DISH = "Search Dish"
let DRIVER_ACCEPT_ORDER = "Driver Accept Order"
let DRIVER_DECLINE_ORDER = "Driver Decline Order"
let DRIVER_PICKUP_FOOD = "Driver Pickup Food"
let DRIVER_DELIVERED_FOOD = "Driver Deliver Food"
let CUSTOMER_CANCEL_ORDER = "Customer Cancel Order"
let ADD_CUSTOMER_CARD_SCREEN = "Add Customer Card Screen"
let DRIVER_CARD_LIST_SCREEN = "Driver Card list screen"
let SELECT_PAYMENT_METHOD_FOR_ORDER_SCREEN = "Select Payment Method"
let PLACE_ORDER_EVENT = "Place Dish Order"
let DRIVER_ADD_STRIPE_ACCOUNT_SCREEN = "Driver Add Stripe Account Screeb"
let CUSTOMER_PROFILE_SCREEN = "Customer Profile Screen"
let DRIVER_PROFILE_SCREEN = "Driver Profile Screen"
let EDIT_PROFILE_SCREEN = "Edit Profile Screen"
let SETTING_SCREEN = "Settings screen"
let CHANGE_PASSWORD_SCREEN = "Change Password Screen"
let ABOUT_US_SCREEN = "About Us Screen"
let SIDE_MENU_SCREEN = "Side Menu Open"
let DISH_REVIEW_SUBMIT = "Dish Review Submit"
let DISH_REVIEW_SKIP = "Dish review skip"
let DISH_REVIEW_LIST_SCREEN = "Dish Review List screen"
let CHEF_MANAGE_DISH_QUANTITY = "Chef Manage Dish Quantity"
let NAVIGATE_MAP_APPLICATION = "Route Direction Event to Google Map"
let TERMS_AND_CONDITION_SCREEN = "Terms and confition Screen"

