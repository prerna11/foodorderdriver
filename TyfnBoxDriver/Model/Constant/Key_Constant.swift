//
////
////  Constant.swift
//  Created by C100-11 on 07/06/17.
//  Copyright © 2017 C100-11. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import SDWebImage
import Mixpanel


let Server_URL = "http://clientapp.narola.online/pg/NafasNewAPI/"

let WS_PATH = "TyfnBoxService.php?Service="

let PROFILE_IMAGE_URL = "\(Server_URL)uploads/user_profile_images/"

let DISH_IMAGE_URL = "\(Server_URL)/Images/Dish_Images/"
let DISH_IMAGE_THUMB_URL = "\(Server_URL)/Images/Dish_Thumbnail/"
let DISH_VIDEO_URL = "\(Server_URL)/videos/Dish_Videos/"

let BaseURL = Server_URL
let ProfileImageBaseURL = ""
let ProfileThumbImageBaseURL = ""

let ABOUT_US_WEB_URL = ""

let TERMS_AND_CONDITION_URL = ""

let EXPRESS_PAYMENT_WEB_URL = ""
//"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tyfnbox.com/TyfnBoxApp/WS/TyfnBox/StripeOAuthCode.php&client_id=ca_C9CcHyS9rOzSKcpOhlA1LAF8lN0AQ1WF&state=" // Client Digital ocean account


let GOOGLE_MAPS_PLACES = "AIzaSyAJYxo4MEGNdGg1PXQUkhwMqE-eaf-rXoM"
let GOOGLE_MAP_AUTOCOMPLETE_URL = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
let GOOGLE_MAP_PLACE_DETAIL_URL = "https://maps.googleapis.com/maps/api/place/details/json"


////MARK : - API Name
//let wsRegister = "DriverRegister"
//let wsRegisterV2 = "DriverRegisterV2"
//let wsLogin = "DriverLogin"
//let wsLoginV2 = "DriverLoginV2"
//let wsChangePassword = "DriverChangePassword"
//let wsEditProfile = "DriverEditProfile"
//let wsEditProfileV2 = "DriverEditProfileV2"
////let wsCurrentOrder = "DriverCurrentOrder" old
//let wsCurrentOrder = "DriverCurrentOrderV4"
//let wsPastOrder = "DriverPastOrder"
//let wsForgotPassword = "ForgotPassword"
////let wsDriverOrderStatus = "DriverOrderStatus" old
//let wsDriverOrderStatus = "DriverOrderStatusV3"
////let wsDriverCurrentOrderRefresh = "DriverCurrentOrderRefresh" old
//let wsDriverCurrentOrderRefresh = "DriverCurrentOrderRefreshV4"
//let wsLogoutUser = "LogoutUser"

//MARK : - API Name
let wsRegisterV2 = "DriverRegister"
let wsLoginV2 = "DriverLogin"
let wsChangePassword = "DriverChangePassword"
let wsEditProfileV2 = "DriverEditProfile"
//let wsCurrentOrder = "DriverCurrentOrder" old
let wsCurrentOrder = "DriverCurrentOrder"
let wsPastOrder = "DriverPastOrder"
let wsForgotPassword = "ForgotPassword"
//let wsDriverOrderStatus = "DriverOrderStatus" old
let wsDriverOrderStatus = "DriverOrderStatusV3"
//let wsDriverCurrentOrderRefresh = "DriverCurrentOrderRefresh" old
let wsDriverCurrentOrderRefresh = "DriverCurrentOrderRefreshV4"
let wsLogoutUser = "LogoutUser"



let wsRefreshToken = "refreshToken"
let wsLoginWithFB = "LoginWithFB"
let wsUserExistWithFacebookID = "UserExistWithFacebookID"
let wsgetPendingChefOrders = "getPendingChefOrders"
let wsgetPendingUserOrders = "getPendingUserOrders"
let wsgetPastUserOrders = "getPastUserOrders"
let wsgetPastChefOrders = "getPastChefOrders"
let wsupdateOrderStatus = "updateOrderStatus"
let wsgetChefDishList = "getChefDishList"
let wsaddDish = "addDish"
let wsEditDishDetail = "EditDishDetail"
let wsgetDishDetail = "getDishDetail"
let wsupdateDishQuantity = "updateDishQuantity"
let wsdeleteDish = "deleteDish"
let wsGetDishStatusDetail = "GetDishStatusDetail"
let wsgetAllDishList = "getAllDishList"
let wsaddDeliveryAddress = "addDeliveryAddress"
let wsgetDeliveryAddressList = "getDeliveryAddressList"
let wsGetCardDetails = "GetCardDetails"
let wsSaveCardDetails = "SaveCardDetails"
let wsplaceOrder = "placeOrder"

let wsGetStripeExpressAccountDetails = "GetStripeExpressAccountDetails"
let wsHandelStripeOauthCode = "HandelStripeOauthCode"

//let userType = UserDefaults.standard.value(forKey: "userType") as! Int
//MARK : - screen size
let screenSize = UIScreen.main.bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height

//MARK : - side menu
let myOrders = "My Orders"
let myDishes = "My Dishes"
let paymentDetail = "Payment Details"
let profile = "Profile"
let feedback = "Feedback"
let settings = "Settings"
let logout = "Logout"
let findDish = "Find a Dish"
let paymentMethod = "Payment Method"

//MARK : - storyboards
let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
let homeStoryboard = UIStoryboard(name: "Home", bundle: nil)
let dishStoryboard = UIStoryboard(name: "Dish", bundle: nil)
let paymentStoryboard = UIStoryboard(name: "Payment", bundle: nil)
let userStoryboard = UIStoryboard(name: "User", bundle: nil)
let settingStoryboard = UIStoryboard(name: "Setting", bundle: nil)
let addressStoryboard = UIStoryboard(name: "Address", bundle: nil)
let ordersStoryboard = UIStoryboard(name: "Orders", bundle: nil)
let chatStoryboard = UIStoryboard(name: "Chat", bundle: nil)

//MARK : - storyboards
let kLoginNavigation = "idLoginNavigation"
let kHomeNavigation = "idHomeNavigationController"
let kMyOrderNavigation = "idMyOrderNavigationController"
let kMyDishNavigation = "idMyDishNavigationController"
let kPaymentDetailNavigation = "idPaymentDetailNavigationController"
let kProfileNavigation = "idProfileNavigationController"
let kFeedbackNavigation = "idFeedbackNavigationController"
let kSettingNavigation = "idSettingNavigationController"
let kFindDishNavigation = "idFindDishNavigationController"
let kCustomerOrdersNavigation = "idCustomerOrdersNavigation"

//MARK : - NSUserDefaults Key
let kDeviceToken = "DeviceToken"
let kToken = "token"
let kGlobalPassword = "globalPassword"
let kCartArray = "cartItems"
let kLoginUser = "LoginUser"

let kSelectedMenu = "selectedMenu"
let kCurrentLocation = "currentLocation"
let kCurrentFullLocation = "currentFullLocation"
let kCurrentLatitude = "currentLatitude"
let kCurrentLongitude = "currentLongitude"
//MARK : - Access and Secret Key
let kNoUsername = "nousername"
let kAccessKey = "access_key"
let kSecretKey = "secret_key"
let kTempToken = "TempToken"
let kUserToken = "UserToken"
let kUserGUID = "UserGUID"

let DEVICE_TYPE = 0
let ISTESTDATA = 1
let STATUS = "status"
let SUCCESS = "success"
let FAILED = "failed"
let USER_ACTIVE = "is_user_active"

let DATETIMEFORMAT = "yyyy-MM-dd HH:mm:ss"
let DATEFORMAT = "yyyy-MM-dd"
let DATEMONTHFORMAT = "MM-dd-yyyy"
//let DATEMONTHFORMAT = "dd MMM yyyy"
let TIMEFORMAT_24 = "HH:mm"
let TIMEFORMAT_12 = "hh:mm a"

let CUSTOMER = 2
let CHEF = 3
let VIDEO_DURATION = 60*5 // in seconds
//MARK : - Placeholder text
let PLACEHOLDER_USERNAME = "User Name"
let PLACEHOLDER_EMAIL = "Email"
let PLACEHOLDER_CONTACT = "Contact"
let PLACEHOLDER_LOCATION = "Location"

//MARK : - Google API key
let GOOGLE_API_KEY = "AIzaSyAJYxo4MEGNdGg1PXQUkhwMqE-eaf-rXoM" //"AIzaSyB3VaGXHWKD9VBFJZXfQCRg1X1LgA4cD7s"

//MARK: - MixPanel Token
let MIXPANEL_TOKEN = "97e0458699a8bb09f51180264759592d" //"7f39a572578982353e105d9c6b9f9d91"
let MIXPANELUSERIDKEY = "UserId"

//MARK : - Time Interval
let TIME_INTERVAL = 60  //Seconds

//MARK : - Color
//extension UIColor{
//    static let ThemeColorRed = UIColor(red: 79.0/255.0, green: 155.0/255.0, blue: 247.0/255.0, alpha: 1.0)
//    static let AppGrayColor = UIColor(red: 163.0/255.0, green: 163.0/255.0, blue: 163.0/255.0, alpha: 1.0)
//    static let TableBackgroundColor = UIColor(red: 236.0/255.0, green: 240.0/255.0, blue: 245.0/255.0, alpha: 1.0)
//}

extension UIColor{
    static let AppRedColor = UIColor(red: 197.0/255.0, green: 33.0/255.0, blue: 40.0/255.0, alpha: 1.0)
    static let AppGrayColor = UIColor(red: 163.0/255.0, green: 163.0/255.0, blue: 163.0/255.0, alpha: 1.0)
    static let TableBackgroundColor = UIColor(red: 236.0/255.0, green: 240.0/255.0, blue: 245.0/255.0, alpha: 1.0)
    static let Greyed = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
}

//MARK : - Order Status Enum
enum OrderStatusEnum:Int {
    case PendingApproveByChef = 1,
    AcceptByChef,
    Processing,
    FinishedCooking,
    CancelledByCustomer,
    RejectByChef,
    Delivered,
    AcceptedByDriver,
    PickedUpByDriver
}
//MARK: - Image
func SET_IMAGE(imageName:String) -> UIImage{return UIImage(named: imageName)!}
func WS_URL(url:String) -> String{return Server_URL+WS_PATH+url}
let DEVICE_TOKEN = AppData.sharedInstance.getDeviceToken() ?? ""
func SHOWPROGRESS(){
    SVProgressHUD.setDefaultMaskType(.black)
    SVProgressHUD.setDefaultAnimationType(.native)
    SVProgressHUD.show()
}
func HIDEPROGRESS(){
    SVProgressHUD.dismiss()
}
func SET_IMAGEURL(IMAGEVIEW:UIImageView,IMAGE:String,PLACEHOLDER:UIImage){
    if let imageURL = NSURL(string:IMAGE) {
        //        IMAGEVIEW.sd_setImage(with: imageURL as URL, placeholderImage: PLACEHOLDER)
        IMAGEVIEW.sd_setImageWithPreviousCachedImage(with: imageURL as URL, placeholderImage: PLACEHOLDER, options: .cacheMemoryOnly, progress: nil, completed: { (image, error, cachType, imageUrl) in
            if image != nil {
                //NSLog(@"Downloaded");
            }
            if cachType != SDImageCacheType.memory {
                IMAGEVIEW.alpha = 0.0
                UIView.animate(withDuration: 0.8, animations: {() -> Void in
                    IMAGEVIEW.alpha = 1.0
                })
            }
        })
    } else {
        IMAGEVIEW.image = PLACEHOLDER
    }
}
func TABLE_ROW_RELOAD(ROW:Int,SECTION:Int,TABLE:UITableView){
    let indexpath = NSIndexPath(row: ROW, section: SECTION)
    TABLE.reloadRows(at: [indexpath as IndexPath], with: .none)
}
func DATE_STR(STR:NSDate,FORMAT:String)->String{
    let dateformat = DateFormatter()
    dateformat.dateFormat = FORMAT
    return dateformat.string(from: STR as Date)
}
func STR_DATE(STR:String,FORMAT:String)->NSDate?{
    let dateformat = DateFormatter()
    dateformat.dateFormat = FORMAT
    return dateformat.date(from: STR) as NSDate?
}
func DATESTR_WITH_MONTHNAME(STR:String) -> String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = DATETIMEFORMAT
    dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
    let date = dateFormatter.date(from: STR)// create   date from string
    
    // change to a readable time format and change to local time zone
    dateFormatter.dateFormat = DATEMONTHFORMAT
    dateFormatter.timeZone = NSTimeZone.local
    return dateFormatter.string(from: date!)
}

func TIMESTR_FROMDATE(STR:String) -> String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = DATETIMEFORMAT
    dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
    let date = dateFormatter.date(from: STR)// create   date from string
    
    // change to a readable time format and change to local time zone
    dateFormatter.dateFormat = TIMEFORMAT_12
    dateFormatter.timeZone = NSTimeZone.local
    return dateFormatter.string(from: date!)
}
func SET_TRACK_EVENT(EVENT:String){
    if(AppData.sharedInstance.getLoginUser() != nil){
        let UserObj = User.init(dictionary: AppData.sharedInstance.getLoginUser()!)
        Mixpanel.mainInstance().track(event: EVENT,
                                      properties: [MIXPANELUSERIDKEY:(UserObj?.id!)!])
    }
    else{
        Mixpanel.mainInstance().track(event: EVENT,
                                      properties: nil)
    }
}
func SET_USER_IDENTIFY(USERID:Int)
{
    Mixpanel.mainInstance().identify(distinctId: String(USERID))
    Mixpanel.mainInstance().people.set(property: MIXPANELUSERIDKEY,
                                       to: String(USERID))
}
