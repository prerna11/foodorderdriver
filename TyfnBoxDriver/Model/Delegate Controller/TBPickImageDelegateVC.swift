//
//  TBPickImageDelegateVC.swift
//  TyfnBox
//
//  Created by C100-11 on 13/11/17.
//  Copyright © 2017 C100-11. All rights reserved.
//

import UIKit

protocol SelectImageDelegate: class {
    func didFinishSelectImage(image:UIImage?,videoURL:NSURL?)
}

class TBPickImageDelegateVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    // MARK: - Variables
    weak var delegate: SelectImageDelegate?
    var view_title:String?
    var fromProfile:Bool?
    let imagePicker = UIImagePickerController()
    
    // MARK: - IBoutlets
    @IBOutlet var lblViewTitle: UILabel!
    @IBOutlet var imgCamera: UIImageView!
    @IBOutlet var lblCameraText: UILabel!
    @IBOutlet var imgGallery: UIImageView!
    @IBOutlet var lblGalleryText: UILabel!
    
    @IBOutlet weak var viewLabel: UIView!
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblViewTitle.text = self.view_title ?? ""
        self.imagePicker.delegate = self
        UINavigationBar.appearance().setBackgroundImage(nil, for: .default)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setThemeUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Delegate / Datasources
    // MARK: - ImagePicker Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        let Image = (info[UIImagePickerControllerOriginalImage] as? UIImage)
        if(Image != nil){
            dismiss(animated: true, completion: nil)
            delegate?.didFinishSelectImage(image: Image, videoURL: nil)
        }else{
            let videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL
            if(videoURL != nil){
                let time = AppData.sharedInstance.getTimeDuration(url: videoURL!)
                if(Int(time) <= VIDEO_DURATION){
                    dismiss(animated: true, completion: nil)
                    delegate?.didFinishSelectImage(image: nil, videoURL: videoURL!)
                }
                else{
                    dismiss(animated: true, completion: nil)
                    let alert = UIAlertController(title: "", message: VIDEO_LIMIT_VALIDATION, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel) { action in
                        // perhaps use action.title here
                        
                        self.delegate?.didFinishSelectImage(image: nil, videoURL: nil)
                    })
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        dismiss(animated: true, completion: nil)
        delegate?.didFinishSelectImage(image: nil, videoURL: nil)
    }
    
    // MARK: - Button Actions
    @IBAction func btnOuterClick(_ sender: UIButton) {
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        delegate?.didFinishSelectImage(image: nil, videoURL: nil)
    }
    
    @IBAction func btnCameraClick(_ sender: UIButton) {
        imgCamera.image = SET_IMAGE(imageName: "Camera_Unselect")
        lblCameraText.textColor = ThemeColor
       // imgGallery.image = SET_IMAGE(imageName: "Gallery_Unselect")
        
        imgGallery.tintColor = ThemeColorGrey
        imgCamera.tintColor = ThemeColor
        lblGalleryText.textColor = ThemeColorGrey
        self.openCamera()
    }
    
    @IBAction func btnGalleryClick(_ sender: UIButton) {
        imgCamera.image = SET_IMAGE(imageName: "Gallery_Unselect")
        lblCameraText.textColor = ThemeColorGrey
        
        imgGallery.tintColor = ThemeColor
        imgCamera.tintColor = ThemeColorGrey
        
        //imgGallery.image = SET_IMAGE(imageName: "Gallery_Select")
        lblGalleryText.textColor = ThemeColor
        self.openGallery()
    }
    
    // MARK: - User Functions
    func openCamera(){
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .camera
        self.imagePicker.cameraCaptureMode = .photo
        self.imagePicker.modalPresentationStyle = .fullScreen
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    func openGallery(){
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .photoLibrary
        if(self.fromProfile != nil && self.fromProfile == false){
            self.imagePicker.mediaTypes = ["public.image", "public.movie"]
        }
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    // MARK: - Navigation
    
    //MARK:-Set Theme Method
    
    func setThemeUI() {
        
        self.viewLabel.backgroundColor = ThemeColor
        self.lblViewTitle.textColor = ThemeColorWhite
        
        
    }
    
}
