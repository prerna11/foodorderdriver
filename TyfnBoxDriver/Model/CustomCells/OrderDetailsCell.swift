//
//  OrderDetailsCell.swift
//  TyfnBoxDriver
//
//  Created by c196 on 22/12/17.
//

import UIKit

class OrderDetailsCell: UITableViewCell {

    // For 1st Cell
    @IBOutlet var imgFood: UIImageView!
    @IBOutlet var lblDishNama: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblDistance: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblQuantity: UILabel!
    @IBOutlet weak var lblTotLabel: UILabel!
    @IBOutlet weak var lblTotQuanLbl: UILabel!
    @IBOutlet weak var lblTotTimeLbl: UILabel!
    
    @IBOutlet weak var lblDateLbl: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    // For 2nd Cell
    @IBOutlet var lblPickUpAdd: UILabel!
    @IBOutlet var lblChefName: UILabel!
    @IBOutlet var lblDropAdd: UILabel!
    @IBOutlet var lblCustomerName: UILabel!
    
    @IBOutlet weak var lblPickAddLbl: UILabel!
    @IBOutlet weak var lblDropOffLbl: UILabel!
    
    @IBOutlet var btnDrpoAdd: UIButton!
    @IBOutlet var btnPickUpAdd: UIButton!
    
    @IBOutlet var lblDropOffDistance: UILabel!
    @IBOutlet var lblPickupDistance: UILabel!
    
    @IBOutlet var btnChefCall: UIButton!
    @IBOutlet var btnCustCall: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setThemeUICell1(){
        //set Color
        self.lblName.textColor = ThemeColorGrey
        self.lblDateLbl.textColor = ThemeColorGrey
        self.lblDishNama.textColor = ThemeColorBlack
        self.lblDate.textColor = ThemeColorBlack
        self.lblTotLabel.textColor = ThemeColorGrey
        self.lblDistance.textColor = ThemeColor
        self.lblTotLabel.textColor = ThemeColorGrey
        self.lblTime.textColor = ThemeColor
        self.lblTotQuanLbl.textColor = ThemeColorGrey
        self.lblQuantity.textColor = ThemeColor
        
        //set Fonts
        self.lblName.font = UIFont(name: Theme_Font_REGULAR, size: 12)
        self.lblDateLbl.font = UIFont(name: Theme_Font_REGULAR, size: 12)
        self.lblDishNama.font = UIFont(name: Theme_Font_REGULAR, size: 14)
        self.lblDate.font = UIFont(name: Theme_Font_REGULAR, size: 14)
        self.lblTotLabel.font = UIFont(name: Theme_Font_REGULAR, size: 12)
        self.lblDistance.font = UIFont(name: Theme_Font_BOLD, size: 15)
        self.lblTotTimeLbl.font = UIFont(name: Theme_Font_REGULAR, size: 12)
        self.lblTime.font = UIFont(name: Theme_Font_BOLD, size: 15)
        self.lblTotQuanLbl.font = UIFont(name: Theme_Font_REGULAR, size: 12)
        self.lblQuantity.font = UIFont(name: Theme_Font_BOLD, size: 15)
        
    }
    
    func setThemeUICell2(){
        //set Color
        self.lblPickAddLbl.textColor = ThemeColor
        self.lblDropOffLbl.textColor = ThemeColor
        self.lblPickUpAdd.textColor = ThemeColorBlack
        self.lblDropAdd.textColor = ThemeColorBlack
        self.lblChefName.textColor = ThemeColorGrey
        self.lblCustomerName.textColor = ThemeColorGrey
        self.lblPickupDistance.textColor = ThemeColor
        self.lblDropOffDistance.textColor = ThemeColor
        
        //set Fonts
        self.lblPickAddLbl.font = UIFont(name: Theme_Font_REGULAR, size: 12)
        self.lblDropOffLbl.font = UIFont(name: Theme_Font_REGULAR, size: 12)
        
        self.lblPickUpAdd.font = UIFont(name: Theme_Font_REGULAR, size: 14)
        self.lblDropAdd.font = UIFont(name: Theme_Font_REGULAR, size: 14)
        
        self.lblPickupDistance.font = UIFont(name: Theme_Font_REGULAR, size: 12)
        self.lblDropOffDistance.font = UIFont(name: Theme_Font_REGULAR, size: 12)
        
        self.lblCustomerName.font = UIFont(name: Theme_Font_REGULAR, size: 12)
        self.lblChefName.font = UIFont(name: Theme_Font_REGULAR, size: 12)
        
    }
    
}
