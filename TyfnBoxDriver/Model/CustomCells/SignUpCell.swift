//
//  SignUpCell.swift
//  TyfnBoxDriver
//
//  Created by c196 on 19/12/17.
//

import UIKit

class SignUpCell: UITableViewCell {

    @IBOutlet var btnProfilePic: UIButton!
    @IBOutlet var tfInput: UITextField!
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var btnSignIn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
