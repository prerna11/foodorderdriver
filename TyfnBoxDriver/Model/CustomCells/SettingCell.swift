//
//  SettingCell.swift
//  TyfnBoxDriver
//
//  Created by c196 on 21/12/17.
//

import UIKit

class SettingCell: UITableViewCell {

    @IBOutlet var lblSettingOption: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
