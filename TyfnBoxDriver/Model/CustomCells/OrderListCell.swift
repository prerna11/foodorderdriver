//
//  OrderListCell.swift
//  TyfnBoxDriver
//
//  Created by c196 on 19/12/17.
//

import UIKit

class OrderListCell: UITableViewCell {

    @IBOutlet var foodVW: UIView!
    @IBOutlet var lblDistance: UILabel!
    @IBOutlet var imgFood: UIImageView!
    @IBOutlet var lblDishName: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var btnAccept: UIButton!
    @IBOutlet var btnReject: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDateLabel: UILabel!
    @IBOutlet var statusVW: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK:-set Theme method
    func setThemeUI() {
        //set Color
        self.lblName.textColor = ThemeColorGrey
        self.lblDateLabel.textColor = ThemeColorGrey
        self.lblDishName.textColor = ThemeColorBlack
        self.lblDate.textColor = ThemeColorBlack
        self.lblDistance.textColor = ThemeColor
        
        //set Fonts
        self.lblName.font = UIFont(name: Theme_Font_REGULAR, size: 11)
        self.lblDateLabel.font = UIFont(name: Theme_Font_REGULAR, size: 11)
        self.lblDishName.font = UIFont(name: Theme_Font_REGULAR, size: 13)
        self.lblDate.font = UIFont(name: Theme_Font_REGULAR, size: 11)
        self.lblDistance.font = UIFont(name: Theme_Font_REGULAR, size: 12)
        
        //Set Images
        self.btnAccept.setBackgroundImage(UIImage(named: IMG_ICON_AcceptColor), for: .normal)
        self.btnReject.setBackgroundImage(UIImage(named: IMG_ICON_RejectGrey), for: .normal)
        
    }
}
