//
//  LoginCell.swift
//  TyfnBoxDriver
//
//  Created by c196 on 18/12/17.
//

import UIKit

class LoginCell: UITableViewCell {
    
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var tfInput: UITextField!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnForgotPassword: UIButton!
//    @IBOutlet var lblSignIn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
