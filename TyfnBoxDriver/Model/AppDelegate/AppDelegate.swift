//
//  AppDelegate.swift
//  TyfnBoxDriver
//
//  Created by c196 on 18/12/17.
//

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import Fabric
import Crashlytics
import Mixpanel

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate {

    var window: UIWindow?
    let locationManager = CLLocationManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        setThemes(type: .app4)
        Fabric.with([Crashlytics.self])
        Mixpanel.initialize(token: MIXPANEL_TOKEN)
        
        GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
        // Override point for customization after application launch.
//        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.shared.enable = true
        DispatchQueue.main.async {
            self.manageLocation()
            self.refreshToken()
        }
        
        let check=UserDefaults.standard.bool(forKey: "Is_userlogin")
        if check == true
        {
            let orderVC = mainStoryboard.instantiateViewController(withIdentifier: "idHomeNavigation") as! UINavigationController
            self.window?.rootViewController = orderVC
            self.window?.makeKeyAndVisible()
        }
        else
        {
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "idLoginNavigation") as! UINavigationController
            self.window?.rootViewController = viewController
            self.window?.makeKeyAndVisible()
        }
        SET_TRACK_EVENT(EVENT: APP_LAUNCH)
        UINavigationBar.appearance().shadowImage = #imageLiteral(resourceName: "NavigationBarShadow")
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: - Location Manager Delegate / Datasources
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        AppData.sharedInstance.setCurrentLatitude(lat: String(locValue.latitude))
        AppData.sharedInstance.setCurrentLongitude(long: String(locValue.longitude))
        self.getAddressFromLatLon(pdblLatitude: String(locValue.latitude), withLongitude: String(locValue.longitude))
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {

        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                else{
                    let pm = placemarks! as [CLPlacemark]
                    
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        /*    print(pm.country)
                         print(pm.locality)
                         print(pm.subLocality)
                         print(pm.thoroughfare)
                         print(pm.postalCode)
                         print(pm.subThoroughfare)*/
                        var addressString : String = ""
                        var fulladdress : String = ""
                        
                        if pm.thoroughfare != nil {
                            fulladdress = fulladdress + pm.thoroughfare!
                        }
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality!
                            fulladdress = fulladdress + pm.subLocality!  + ", "
                        }
                        if pm.locality != nil {
                            fulladdress = fulladdress + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            fulladdress = fulladdress + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            fulladdress = fulladdress + pm.postalCode!
                        }
                        addressString = addressString.trim()
                        if(addressString.characters.first == ",")
                        {
                            addressString.remove(at: addressString.startIndex)
                        }
                        
                        addressString = addressString.trim()
                        if(fulladdress.characters.first == ",")
                        {
                            fulladdress.remove(at: fulladdress.startIndex)
                        }
                        AppData.sharedInstance.setCurrentLocation(str: addressString)
                        AppData.sharedInstance.setCurrentFullLocation(str: fulladdress)
                    }
                }
        })
    }
    
    // MARK: - setup UI
    func manageLocation(){
//        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func setupLoginUI(){
        let loginNavigationController = mainStoryboard.instantiateViewController(withIdentifier: kLoginNavigation) as! UINavigationController
        self.window?.rootViewController = loginNavigationController
    }
    
    // MARK: - Refresh Token
    func refreshToken(){
        let param = ["access_key":"nousername"]
        
        //        WebserviceConnector.sharedInstance.begin(withURLString: WS_URL(url: wsRefreshToken), withParameters: param as NSDictionary, withSelector: #selector(self.refreshTokenResponse(sender:)), withTarget: (window?.rootViewController!)!, forServiceType: .JSON, andErrorMessage: "failed")
        APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsRefreshToken), parameters: param as NSDictionary) { (response,error) in
            if(response != nil){
                let status = response?.value(forKey: "status") as! String
                if(status == FAILED){
                    print(response ?? "")
                }
                else{
                    if(status == SUCCESS)
                    {
                        //                        let adminConfig = response?.value(forKey: "Result") as? NSDictionary
                        let golbalPassword = response?.value(forKey: "global_password") as! String
                        AppData.sharedInstance.setGlobalPassword(pwd: golbalPassword)
                        let token = response?.value(forKey: "TempToken") as? String
                        if(token != nil){
                            AppData.sharedInstance.setTempToken(token: token!)
                        }
                    }
                }
            }
            else{
                print(error?.localizedDescription ?? "")
            }
        }
    }
    
    @objc func refreshTokenResponse(sender: AnyObject){
        if let senderConnector = sender as? WebserviceConnector
        {
            if senderConnector.responseCode == 100 {
                let dictResponse = senderConnector.responseDictionary
                self.setResponse(dictResponse: dictResponse!)
            }
            else if senderConnector.responseCode == 104 {
                AppData.sharedInstance.showNoInternetMessage(view: (window?.rootViewController)!)
            }
        }
    }
    
    func setResponse(dictResponse:NSDictionary){
        let status = dictResponse.value(forKey: "status") as! String
        if(status == FAILED){
            print(dictResponse)
        }
        else{
            if(status == SUCCESS)
            {
                //                let adminConfig = dictResponse.value(forKey: "Result") as? NSDictionary
                let golbalPassword = dictResponse.value(forKey: "global_password") as! String
                AppData.sharedInstance.setGlobalPassword(pwd: golbalPassword)
                let token = dictResponse.value(forKey: "TempToken") as? String
                if(token != nil){
                    AppData.sharedInstance.setTempToken(token: token!)
                }
            }
        }
    }
}

extension String {
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
}
