//
//  APIUtility.swift
//  Habitator
//
//  Created by C100-11 on 07/06/17.
//  Copyright © 2017 C100-11. All rights reserved.
//

import UIKit
import Alamofire

class APIUtility: NSObject {
    static let sharedInstance = APIUtility()
    
    //MARK: - Variables
    
    //MARK: - GET Method API
    
    func getDetail(completion: @escaping (NSDictionary?) -> Void) {
        Alamofire.request(
            URL(string: BaseURL)!,
            method: .get,
            parameters: ["":""])
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    completion(nil)
                    return
                }
                let responseData = response.result.value as? NSDictionary
                if(responseData != nil){
                    completion(responseData)
                }
                completion(nil)
        }
    }
    func getFBImageDetail(url:String,completion: @escaping (NSDictionary?) -> Void) {
        Alamofire.request(
            URL(string: url)!,
            method: .get,
            parameters: ["":""])
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    completion(nil)
                    return
                }
                let responseData = response.result.value as? NSDictionary
                if(responseData != nil){
                    completion(responseData)
                }
                completion(nil)
        }
    }

    //MARK: - POST Method API
    
    func PostDetail(url:String,parameters:NSDictionary,completion: @escaping (NSDictionary?,NSError?) -> Void) {
//        if(Reachability.NetworkStatus == Reachability.NetworkStatus.notReachable){
//
//        }
        let url = URL(string: url)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        
        let parameters = parameters
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch {
            // No-op
            _ = UIWindow(frame: UIScreen.main.bounds)            
            print(error)
        }
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("iOS", forHTTPHeaderField: "User-Agent")
        
        Alamofire.request(urlRequest).responseJSON { response in
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.result.error!)
                completion(nil,response.result.error as NSError?)
                return
            }
            let responseData = response.result.value as? NSDictionary
            if(responseData != nil){
                completion(responseData,nil)
            }
            completion(nil,response.result.error as NSError?)
         }
    }
    
    func FBPostDetail(url:String,parameters:NSDictionary,completion: @escaping (NSDictionary?,NSError?) -> Void) {
        let url = URL(string: url)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        
        let parameters = parameters
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch {
            // No-op
            _ = UIWindow(frame: UIScreen.main.bounds)
            print(error)
        }
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("iOS", forHTTPHeaderField: "User-Agent")
        
        Alamofire.request(urlRequest).responseJSON { response in
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.result.error!)
                completion(nil,response.result.error as NSError?)
                return
            }
            let responseData = response.result.value as? NSDictionary
            if(responseData != nil){
                completion(responseData,nil)
            }
            completion(nil,response.result.error as NSError?)
        }
    }
    
    //MARK: - Upload data on URL
    func uploadData(url:String,parameters:NSDictionary,completion: @escaping (NSDictionary?,NSError?) -> Void) {
        
    }
}
