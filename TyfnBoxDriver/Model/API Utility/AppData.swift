//
//  AppData.swift
//  Habitator
//
//  Created by C100-11 on 07/06/17.
//  Copyright © 2017 C100-11. All rights reserved.
//
//test

import UIKit
import RMMapper
import SVProgressHUD
import AVFoundation
import CoreLocation

class AppData: NSObject {
    
    static let sharedInstance = AppData()
    let bounds = UIScreen.main.bounds
    let UserDefaultsManager = UserDefaults.standard
    var languageBundle:Bundle!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //MARK: - Manage logout if account is deactive
    func MANAGE_DEACTIVE_ACCOUNT_FLOW(controller:UIViewController,message:String){
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
            self.logoutAPI(controller: controller)
        })
        controller.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Logout API
    func logoutAPI(controller:UIViewController){
        let dic = self.getLoginUser()
        if(dic != nil){
            let userObj = User.init(dictionary: dic!)!
            let param = [    "userId":userObj.id!
            ]
            APIUtility.sharedInstance.PostDetail(url: WS_URL(url: wsLogoutUser), parameters: param as NSDictionary, completion: { (response, error) in
                if(error == nil){
                    self.logoutResponse(dictResponse: response, controller: controller)
                }
                else{
                    HIDEPROGRESS()
                }
            })
        }
    }
    func logoutResponse(dictResponse: NSDictionary?,controller:UIViewController){
        print(dictResponse ?? "")
        HIDEPROGRESS()
        if(dictResponse != nil){
            let status = dictResponse?.value(forKey: STATUS) as! String
            if(status == SUCCESS){
                logOut()
            }
            else{
                AppData.sharedInstance.showAlertMessage(title: "Error", Message: FAILED_MESSAGE, view: controller)
            }
        }
    }
    
    func logOut(){
        self.removeLogoutKey()
        UserDefaults.standard.removeObject(forKey: "userType")
        DispatchQueue.main.async {
            UserDefaults.standard.set(false, forKey: "Is_userlogin")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.application(UIApplication.shared, didFinishLaunchingWithOptions: nil)
        }
    }
    
    //MARK: - Show/hide  Progress
    func showProgress(){
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.setDefaultAnimationType(.flat)
        SVProgressHUD.show()
    }
    func hideProgress(){
        SVProgressHUD.dismiss()
    }
    //MARK: - Check location service enabled
    func checkLocationServiceEnable(view:UIViewController){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                AppData.sharedInstance.setCurrentLatitude(lat: "25.761680")
                AppData.sharedInstance.setCurrentLongitude(long: "-80.191790")
                AppData.sharedInstance.setCurrentLocation(str: "Miami")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                APP_DELEGATE.locationManager.requestWhenInUseAuthorization()
                APP_DELEGATE.locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
            AppData.sharedInstance.setCurrentLatitude(lat: "25.761680")
            AppData.sharedInstance.setCurrentLongitude(long: "-80.191790")
            AppData.sharedInstance.setCurrentLocation(str: "Miami")
        }
    }
    func checkLocationService()->Bool{
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                AppData.sharedInstance.setCurrentLatitude(lat: "25.761680")
                AppData.sharedInstance.setCurrentLongitude(long: "-80.191790")
                AppData.sharedInstance.setCurrentLocation(str: "Miami")
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                return true
            }
        } else {
            print("Location services are not enabled")
            AppData.sharedInstance.setCurrentLatitude(lat: "25.761680")
            AppData.sharedInstance.setCurrentLongitude(long: "-80.191790")
            AppData.sharedInstance.setCurrentLocation(str: "Miami")
            return false
        }
    }
    //MARK: - Get Parameter Value from URL string
    func getQueryStringParameter(url: String, param: String) -> String? {
        let url = NSURLComponents(string: url)!
        return
            (url.queryItems! as [NSURLQueryItem])
                .filter({ (item) in item.name == param }).first?
                .value
    }
    //MARK: - GET/SET Device Token
    func setDeviceToken(token:String){
        UserDefaults.standard.set(token, forKey:kDeviceToken) //setObject
        UserDefaults.standard.synchronize()
    }
    func getDeviceToken() -> String? {
        let token = UserDefaults.standard.value(forKey: kDeviceToken) as? String
        return token
    }
    func setSelectedMenu(id:Int){
        UserDefaults.standard.set(id, forKey:kSelectedMenu) //setObject
        UserDefaults.standard.synchronize()
    }
    func getSelectedMenu() -> Int? {
        let menu = UserDefaults.standard.value(forKey: kSelectedMenu) as? Int
        return menu
    }
    func removeSelectedMenu() {
        UserDefaults.standard.removeObject(forKey: kSelectedMenu)
    }
    func setCurrentLocation(str:String){
        UserDefaults.standard.set(str, forKey:kCurrentLocation) //setObject
        UserDefaults.standard.synchronize()
    }
    func getCurrentLocation() -> String? {
        let location = UserDefaults.standard.value(forKey: kCurrentLocation) as? String
        return location
    }
    func removeCurrentLocation() {
        UserDefaults.standard.removeObject(forKey: kCurrentLocation)
    }
    func setCurrentFullLocation(str:String){
        UserDefaults.standard.set(str, forKey:kCurrentFullLocation) //setObject
        UserDefaults.standard.synchronize()
    }
    func getCurrentFullLocation() -> String? {
        let location = UserDefaults.standard.value(forKey: kCurrentFullLocation) as? String
        return location
    }
    func removeCurrentFullLocation() {
        UserDefaults.standard.removeObject(forKey: kCurrentFullLocation)
    }
    func setCurrentLatitude(lat:String){
        UserDefaults.standard.set(lat, forKey:kCurrentLatitude) //setObject
        UserDefaults.standard.synchronize()
    }
    func getCurrentLatitude() -> String? {
        let location = UserDefaults.standard.value(forKey: kCurrentLatitude) as? String
        return location
    }
    func removeCurrentLatitude() {
        UserDefaults.standard.removeObject(forKey: kCurrentLatitude)
    }
    func setCurrentLongitude(long:String){
        UserDefaults.standard.set(long, forKey:kCurrentLongitude) //setObject
        UserDefaults.standard.synchronize()
    }
    func getCurrentLongitude() -> String? {
        let location = UserDefaults.standard.value(forKey: kCurrentLongitude) as? String
        return location
    }
    func removeCurrentLongitude() {
        UserDefaults.standard.removeObject(forKey: kCurrentLongitude)
    }
    
    func setUserType(type:Int) {
        UserDefaults.standard.setValue(type, forKey: "userType")
        UserDefaults.standard.synchronize()
    }
    func userType() -> Int {
        let type = UserDefaults.standard.value(forKey: "userType") as! Int
        return type
    }
    func setLoginUser(user:NSDictionary){
        UserDefaults.standard.rm_setCustomObject(user, forKey: kLoginUser)
        UserDefaults.standard.synchronize()
    }
    func getLoginUser() -> NSDictionary? {
        let user = UserDefaults.standard.rm_customObject(forKey: kLoginUser) as? NSDictionary
        return user
    }
    func removeLogoutKey(){
        AppData.sharedInstance.removeSelectedMenu()
        UserDefaults.standard.removeObject(forKey: kUserToken)
        UserDefaults.standard.removeObject(forKey: kLoginUser)
        UserDefaults.standard.synchronize()
    }
    //MARK: - Email Validation
    func setRefreshControl(refreshController:UIRefreshControl,table:UITableView){
        if #available(iOS 10.0, *) {
            table.refreshControl = refreshController
        } else {
            table.addSubview(refreshController)
        }
        refreshController.tintColor = ThemeColor
    }
    //MARK: - Email Validation
    func checkEmailValidation(email:String) -> Bool {
        let emailRegEx = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    func convertBase64String(img:UIImage) -> String{
        let imageData:NSData = UIImageJPEGRepresentation(img, 1.0)! as NSData
        return imageData.base64EncodedString(options: .lineLength64Characters)
    }
    //MARK: - Show/hide  Progress
    /* func showProgress(){
     SVProgressHUD.setDefaultMaskType(.gradient)
     SVProgressHUD.show()
     }
     func hideProgress(){
     SVProgressHUD.dismiss()
     }
     */
    //MARK: - Show  Alert
    func showAlertMessage(title:String, Message:String, view:UIViewController)
    {
        let alertView = UIAlertController(title: title, message: Message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        view.present(alertView, animated: true, completion: nil)
    }
    func showNoInternetMessage(view:UIViewController){
        self.showAlertMessage(title: "", Message:"No InterNet Connection", view: view)
    }
    //MARK: - Access Key and Secret Key Convert Base64
    func getAccessKey() -> String
    {
        var accesskey = ""
        if(UserDefaultsManager.value(forKey: kLoginUser) != nil)
        {
            //                        accesskey = "generatedaccesskey"
//            let userObj = User.init(dictionary: self.getLoginUser()!)
//            if(AppData.sharedInstance.getGlobalPassword() != nil){
//                accesskey = FBEncryptorAES.encryptBase64String(userObj?.guid!, keyString: AppData.sharedInstance.getGlobalPassword()!, separateLines: false)
            }
            else{
                accesskey = kNoUsername
            }
        return accesskey
    }
    func encryptBase64String(str:String) -> String{
        let data = (str).data(using: String.Encoding.utf8)
        let base64 = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        print("Encoded:  \(base64)")
        return base64
    }
    func decryptBase64String(str:String) -> String{
        if let base64Decoded = NSData(base64Encoded: str, options:   NSData.Base64DecodingOptions(rawValue: 0))
            .map({ NSString(data: $0 as Data, encoding: String.Encoding.utf8.rawValue) })
        {
            // Convert back to a string
            print("Decoded:  \(String(describing: base64Decoded))")
            return base64Decoded! as String
        }
        return ""
    }
    
    //MARK: - GET/SET Token String
    func setGlobalPassword(pwd:String){
        UserDefaults.standard.set(pwd, forKey:kGlobalPassword) //setObject
        UserDefaults.standard.synchronize()
    }
    func getGlobalPassword() -> String? {
        let gPWD = UserDefaults.standard.value(forKey: kGlobalPassword) as? String
        return gPWD
    }
    func setUserToken(token:String){
        UserDefaults.standard.set(token, forKey:kUserToken) //setObject
        UserDefaults.standard.synchronize()
    }
    func getUserToken() -> String? {
        let token = UserDefaults.standard.value(forKey: kUserToken) as? String
        return token
    }
    func setTempToken(token:String){
        UserDefaults.standard.set(token, forKey:kToken) //setObject
        UserDefaults.standard.synchronize()
    }
    func getTempToken() -> String? {
        let token = UserDefaults.standard.value(forKey: kToken) as? String
        return token
    }
    /*   func setLoginUser(user:NSDictionary){
     UserDefaults.standard.rm_setCustomObject(user, forKey: kLoginUser)
     UserDefaults.standard.synchronize()
     }
     func getLoginUser() -> NSDictionary? {
     let user = UserDefaults.standard.rm_customObject(forKey: kLoginUser) as? NSDictionary
     return user
     }*/
    
    //MARK: - Get Thumnail from URL
    func thumbnailForVideoAtURL(url: NSURL) -> UIImage? {
        
        let asset = AVAsset.init(url: url as URL)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    //MARK: - Get video time from URL
    func getTimeDuration(url: NSURL) ->  Float64{
        let asset = AVAsset.init(url: url as URL)
        let duration = asset.duration
        let durationTime = CMTimeGetSeconds(duration)
        print(durationTime)
        return durationTime
    }
    //MARK: - Get App Language
    func appLanguage()-> String{
        let preferredLanguage = NSLocale.preferredLanguages[0]
        return preferredLanguage
    }
    //MARK: - Get Device Height and width
    func getDeviceWidth()->CGFloat{
        let width = bounds.size.width
        return width
    }
    func getDeviceHeight()->CGFloat{
        let height = bounds.size.height
        return height
    }
    //MARK: - GET Json String from NSDictionary
    
    func getJsonString(arrayDictionary:NSMutableArray) -> String {
        do{
            let jsonData : NSData = try JSONSerialization.data(withJSONObject: arrayDictionary, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
            let json = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                return json as String
            }
        }
        catch{}
        return ""
    }
    //MARK: - Set View shadow
    func setBorderShadow(view:UIView){
        let shadowSize : CGFloat = 1.0
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: view.frame.size.width + shadowSize,
                                                   height: view.frame.size.height + shadowSize))
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowOpacity = 0.4
        view.layer.shadowPath = shadowPath.cgPath
    }
    func setViewBorder(view:UIView,color:UIColor){
        view.layer.borderWidth = 1.0
        view.layer.borderColor = color.cgColor
    }
    func setImageBorder(view:UIImageView){
        view.layer.borderWidth = 1.0
        view.layer.borderColor = ThemeColor.cgColor
    }
    //MARK : - Round Avatar Image
    func roundAvatarImage(imageview:UIImageView){
        imageview.layer.cornerRadius = imageview.frame.size.width / 2
        imageview.clipsToBounds = true
    }
    
    //MARK: - TextBox Validation
    func textBoxValidation(view:UIView){
        let shadowSize : CGFloat = 0.5
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: view.frame.size.width + shadowSize,
                                                   height: view.frame.size.height + shadowSize))
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.red.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowPath = shadowPath.cgPath
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: view.center.x - 10, y: view.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: view.center.x + 10, y: view.center.y))
        view.layer.add(animation, forKey: "position")
    }
    
    //MARK: - Empty Tableview
    func setEmptyTableView(tableView:UITableView,message:String,imgName:String)
    {
        let EmptyView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        let centerView : UIView = UIView(frame: CGRect(x: 20, y: 0, width: tableView.bounds.size.width - 20, height: 200))
        let image : UIImageView = UIImageView(frame: CGRect(x: 50, y: 0, width: centerView.bounds.size.width - 100, height: 100))
        image.image = UIImage(named: imgName)
        image.contentMode = .scaleAspectFit
        let emptyTableLabel1:UILabel = UILabel(frame: CGRect(x: 5, y: image.bounds.size.height/2, width: centerView.bounds.size.width - 10, height: 50))
        
        emptyTableLabel1.text = message
        emptyTableLabel1.textAlignment = .center
        emptyTableLabel1.numberOfLines = 2

        emptyTableLabel1.textColor     = UIColor.black
        emptyTableLabel1.textAlignment = .center
        centerView.center = EmptyView.center
        centerView.backgroundColor = UIColor.clear
        
        EmptyView.backgroundColor = UIColor.clear
        
        centerView.addSubview(image)
        centerView.addSubview(emptyTableLabel1)
        EmptyView.addSubview(centerView)
        
        tableView.backgroundView  = EmptyView
        tableView.separatorStyle  = .none
    }
}

extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

extension UIButton{
        func topRoundedButton(){
            let maskPAth1 = UIBezierPath(roundedRect: self.bounds,
                                         byRoundingCorners: [.topRight],
                                         cornerRadii:CGSize(width: 4.0, height: 4.0))
            let maskLayer1 = CAShapeLayer()
            maskLayer1.frame = self.bounds
            maskLayer1.path = maskPAth1.cgPath
            self.layer.mask = maskLayer1
    }    
}

extension UIButton{
    func BottomRoundedButton(){
        let maskPAth1 = UIBezierPath(roundedRect: self.bounds,
                                     byRoundingCorners: [.bottomRight],
                                     cornerRadii:CGSize(width: 4.0, height: 4.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = self.bounds
        maskLayer1.path = maskPAth1.cgPath
        self.layer.mask = maskLayer1
    }
}

