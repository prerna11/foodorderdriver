//
//  WebserviceResponse.swift
//  NIPLFrameworkSwift
//
//  Created by C237 on 07/07/17.
//  Copyright © 2017 developer.nipl. All rights reserved.
//

import Foundation

class WebserviceResponse
{
    static let sharedInstance = WebserviceResponse()
    var operationQueue = OperationQueue()
    
    
    init()
    {
        operationQueue.maxConcurrentOperationCount = 1
    }
    
    func initTask(request:NSMutableURLRequest, responseData:@escaping (_ error:Error? ,_ responseObjects: NSDictionary?,_ responseString:String) -> Void)
    {
        let operation = BlockOperation.init {
            DispatchQueue.global(qos: .background).async {
                print("PROCESSING REQUEST ...")
                let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                    var responseValues : NSDictionary? = nil
                    var responseString = "Successful"
                    if(data != nil && (data?.count)! > 0)
                    {
                        responseString = String(data: data!, encoding: .ascii)!
                        do
                        {
                            let allValues = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary
                            if allValues!.count <= 0
                            {
                                responseString = "No Data Available";
                            }
                            responseValues = allValues
                        }
                        catch let errorJson as NSError
                        {
                            //WHEN JSON IS NOT IN CORRECT FORMAT - OR SERVER IS DOWN (SIMPLY NOT RECEIVED RESPONSE IN JSON)
                            print(errorJson)
                            responseString = "NIL"
                            //responseData(errorJson, responseValues, responseString)
                        }
                    }
                    else if(error != nil)
                    {
                        /*CONNECTION IS SLOW SO NOT RECEIVED RESPONSE _ TIMEOUT */
                        responseString = "Fail"
                    }
                    else if((data?.count)! == 0)
                    {
                        /*ERROR IN API OR NOT RECEIVED RESPONSE*/
                        responseString = "No Data Available";
                    }
                    else
                    {
                        /*ANY UNKNOWN ERROR*/
                        responseString = "NIL"
                    }
                    DispatchQueue.main.async {
                        print("This is run on the main queue, after the previous code in outer block")
                        responseData(error, responseValues, responseString)
                    }
                }
                task.resume()
            }
        }
        
        operation.queuePriority = .normal
        self.operationQueue.addOperation(operation)
        
    }
    
    
}
