//
//  WebserviceConnector.swift
//  NIPLFrameworkSwift
//
//  Created by C237 on 07/07/17.
//  Copyright © 2017 developer.nipl. All rights reserved.
//

import Foundation
import UIKit

class WebserviceConnector
{
    var responseCode = 100;
    var responseError = ""
    var responseDictionary : NSDictionary!
    var responseArray : NSArray!
    var URLRequest = NSMutableURLRequest()
    var calledFrom : UIViewController!
    
    static let sharedInstance = WebserviceConnector()
    
    
    func checkInternetConnection() -> Bool
    {
        return ReachabilityManager.sharedInstance.isReachable()
    }
    
    func begin(withURLString urlString:String,
               withParameters paramDictionary:NSDictionary?,
               withSelector selector:Selector,
               withTarget target:UIViewController,
               forServiceType serviceType:ServiceType,
               andErrorMessage errorMsg:String,
               mediaData:Data? = nil,
               mediaType:MediaType? = nil)
    {
        self.calledFrom = target
        NIPLFunctions.showNetworkIndicator(xx: true)
        switch serviceType
        {
        case .GET:
            URLRequest = getMutableRequestForGETWS(url: urlString, params: paramDictionary)
        case .POST:
            URLRequest = getMutableRequestForPOSTWS(url: urlString, params: paramDictionary)
        case .JSON:
            URLRequest = getMutableRequestForJSONWS(url: urlString, params: paramDictionary)
        case .MEDIA:
            if(mediaData != nil && mediaType != nil)
            {
                URLRequest = getMutableRequestForPOSTMEDIAWS(url: urlString, params: paramDictionary, mediaData: mediaData!, mediaType: mediaType!)
            }
            else
            {
                print("ERROR :- MEDIA TYPE CANNOT HAVE MEDIA DATA AND TYPE PARAMETER AS NIL")
                return
            }
        }
        
        print("REACHABILITY STATUS = \(String(describing: Reachability.init()?.currentReachabilityStatus))")
        
        if(ReachabilityManager.sharedInstance.isReachable())
        {
            print("IS REACHABLE ")
            
            //START LOADER
            if !JustHUD.shared.isActive
            {
                JustHUD.shared.showInView(view: calledFrom.view)
                print("Showing HUD is currently hidden")
            } else {
                print("Showing Currently showing HUD")
            }
            
            
            NIPLFunctions.showNetworkIndicator(xx: true)
            self.responseCode = 100;
            self.responseError = ""
            self.responseArray = NSArray()
            self.responseDictionary = NSDictionary()
            
            
            WebserviceResponse.sharedInstance.initTask(request: URLRequest, responseData: { (error, responseObj, responseString) in
                NIPLFunctions.showNetworkIndicator(xx: false)
                
                if(error != nil)
                {
                    self.responseCode = 101;
                    self.responseError = (error?.localizedDescription)!;
                }
                else
                {
                    if(responseString == "Fail" || responseString == "NIL")
                    {
                        self.responseCode = 102;
                        self.responseError = "Response Issue From Server";
                    }
                    else if(responseString == "No Data Available")
                    {
                        self.responseCode = 103;
                        self.responseError = "Response Issue From Server";
                    }
                        /*
                    else if (responseObj?.value(forKey: "status") as! String == "failed")
                    {
                        self.responseCode = 104;
                        self.responseError = responseObj?.value(forKey: "message") as! String
                    }
 */
                    else
                    {
                        self.responseCode = 100;
                        self.responseDictionary = responseObj!
                        //parse array from core data
                    }
                }
                
                runOnMainThreadWithoutDeadlock
                {
                    //END LOADER
                    if JustHUD.shared.isActive
                    {
                        JustHUD.shared.hide()
                        print("Hiding Currently showing HUD")
                    } else {
                        print("Hiding HUD is currently hidden")
                    }
                    
                    
                    target.performSelector(onMainThread: selector, with: self, waitUntilDone: false)
                }
            })
        }
        else
        {
            NIPLFunctions.showNetworkIndicator(xx: false)
            self.responseCode = 104;
            self.responseError = "The network connection was lost."
            target.performSelector(onMainThread: selector, with: self, waitUntilDone: false)
        }
    }
    
    func getMutableRequestForGETWS(url:String,params:NSDictionary?) -> NSMutableURLRequest
    {
        TraceOperations.traceWS(requestURL: url, paramDict: params, serviceType: "GET", calledFrom: self.calledFrom)
        var query = "";
        if(params == nil)
        {
            query = url;
        }
        else
        {
            query += "?"+getParamKeyValueString(params: params)
        }
        query = query.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!
        let urlQuery = URL(string: query)
        let request = NSMutableURLRequest(url: urlQuery!, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: DEFAULT_TIMEOUT)
        return request
    }
    
    func getMutableRequestForPOSTWS(url:String,params:NSDictionary?) -> NSMutableURLRequest
    {
        TraceOperations.traceWS(requestURL: url, paramDict: params, serviceType: "POST", calledFrom: self.calledFrom)
        let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        let urlQuery = URL(string: urlString!)
        let request = NSMutableURLRequest(url: urlQuery!, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: DEFAULT_TIMEOUT)
        
        let postString = getParamKeyValueString(params: params)
        let postData:Data? = postString.data(using: .utf8)
        _ = String(postData!.count)
        request.httpMethod = "POST"
        request.httpBody = postData
        
        //request.addValue(postLength, forHTTPHeaderField: "Content-Length")
        //request.addValue("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
//        request.addValue(UserDefaults.standard.value(forKey: ACCESS_TOKEN) as! String, forHTTPHeaderField: "Authorization")

        return request
    }
    
    func getMutableRequestForJSONWS(url:String,params:NSDictionary?) -> NSMutableURLRequest
    {
        TraceOperations.traceWS(requestURL: url, paramDict: params, serviceType: "JSON", calledFrom: self.calledFrom)
        let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        let urlQuery = URL(string: urlString!)
        let request = NSMutableURLRequest(url: urlQuery!, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: DEFAULT_TIMEOUT)
        
        let objData = self.dictionaryToJsonData(dictToConvert: params!)
        request.httpMethod = "POST"
        request.httpBody = objData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        return request
    }
    
    func getMutableRequestForPOSTMEDIAWS(url:String,params:NSDictionary?,mediaData:Data?,mediaType: MediaType) -> NSMutableURLRequest
    {
        TraceOperations.traceWS(requestURL: url, paramDict: params, serviceType: "POST", calledFrom: self.calledFrom)
        let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        let urlQuery = URL(string: urlString!)
        let request = NSMutableURLRequest(url: urlQuery!, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: DEFAULT_TIMEOUT)
        
        //var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = createBodyWithParams(params as! [String : Any]?, filePathKey: "file", imageDataKey: mediaData!, boundary: boundary,mediaType:mediaType)
        
        return request
    }
    
    //Custom body of HTTP request to upload image file
    func createBodyWithParams(_ parameters: [String: Any]?, filePathKey: String?, imageDataKey: Data, boundary: String,mediaType: MediaType) -> Data
    {
        let body = NSMutableData();
        if parameters != nil
        {
            for (key, value) in parameters!
            {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        let filename = "tempFileName"//"tempFileName.jpg"
        let mimetype = NIPLFunctions.getMimeTypeForMediaType(type: mediaType)//"image/jpg"
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        return body as Data
    }

    
    //MARK:- Helper Methods
    
    func getParamKeyValueString(params:NSDictionary?) -> String
    {
        var paramsString = ""
        for (key, value) in params!
        {
            if paramsString != ""
            {
                paramsString += "&"
            }
            paramsString += "\(key)=\(value)"
        }
        return paramsString
    }
    
    func dictionaryToJsonData(dictToConvert:NSDictionary) -> Data?
    {
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: dictToConvert, options: .prettyPrinted)
            return jsonData
        }
        catch
        {
            return nil
        }
    }
}

extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

