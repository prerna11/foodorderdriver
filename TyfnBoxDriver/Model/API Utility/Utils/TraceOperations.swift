//
//  TraceOperations.swift
//  NIPLFrameworkSwift
//
//  Created by C237 on 11/07/17.
//  Copyright © 2017 developer.nipl. All rights reserved.
//

import Foundation
import UIKit
class TraceOperations
{
    let ERROR_LOG_FILE = "ErrorLogs.txt"
    let WEBSERVICE_LOG_FILE = "WSLogs.txt"
    let NAVIGATION_FLOW_LOG_FILE = "NavFlowLogs.txt"
    
    static let sharedInstance = TraceOperations()
 
    
    class func traceWS(requestURL:String,paramDict:NSDictionary?,serviceType:String,calledFrom:UIViewController)
    {
        let className = NSStringFromClass(calledFrom.classForCoder)
        TraceOperations.sharedInstance.writeWebserviceLogsToFile(str: "================\n Trace \(NSDate()) \nService URL : \(requestURL) \nService Type : \(serviceType) \nParameters : \(paramDict) \nCalledFrom : \(className)")
    }
    
    
    
    func writeErrorLogsToFile(strErr:String)
    {
        self.writeToFile(str: strErr, fileName: ERROR_LOG_FILE)
    }
    
    func writeNavigationLogsToFile(str:String)
    {
        self.writeToFile(str: str, fileName: NAVIGATION_FLOW_LOG_FILE)
    }
    
    func writeWebserviceLogsToFile(str:String)
    {
        self.writeToFile(str: str, fileName: WEBSERVICE_LOG_FILE)
    }
    
    func readErrorLogsToFile(strErr:String)
    {
        print(self.readLogs(fromFileName: ERROR_LOG_FILE) ?? "no error logs")
    }
    
    func readNavigationLogsToFile(strErr:String)
    {
        print(self.readLogs(fromFileName: NAVIGATION_FLOW_LOG_FILE) ?? "no navigation logs")
    }
    
    func readWebserviceLogsToFile(strErr:String)
    {
        print(self.readLogs(fromFileName: WEBSERVICE_LOG_FILE) ?? "no webservice logs")
    }
    
    
    func writeToFile(str:String,fileName:String)
    {
        let dir = FileManager.default.urls(for: FileManager.SearchPathDirectory.cachesDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first!
        let fileurl =  dir.appendingPathComponent(fileName)
        
        let data = str.data(using: .utf8, allowLossyConversion: false)!
        
        if FileManager.default.fileExists(atPath: fileurl.path)
        {
            if let fileHandle = try? FileHandle(forUpdating: fileurl)
            {
                fileHandle.seekToEndOfFile()
                fileHandle.write(data)
                fileHandle.closeFile()
            }
        }
        else
        {
            try! data.write(to: fileurl, options: Data.WritingOptions.atomic)
        }
    }
    
    func readLogs(fromFileName:String) -> String?
    {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let path = dir.appendingPathComponent(fromFileName)
            do {
                let text2 = try String(contentsOf: path, encoding: String.Encoding.utf8)
                return text2
            }
            catch {
                return nil
            }
        }
        return nil
    }
}
