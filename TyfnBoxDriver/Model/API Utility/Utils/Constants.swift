//
//  Constants.swift
//  NIPLFrameworkSwift
//
//  Created by C237 on 30/10/17.
//  Copyright © 2017 developer.nipl. All rights reserved.
//

import Foundation
import UIKit

//let Server_URL = "http://192.168.1.237/"
//let MEDIA_URL = "\(Server_URL)LibraryManager/Images/"
//let Server_URL = "http://clientapp.narola.online/pg/TyfnBox/"
//let WS_PATH = "TyfnBoxService.php?Service="
//let PROFILE_IMAGE_URL = "\(Server_URL)/Images/Profile_Images/"
//let DISH_IMAGE_URL = "\(Server_URL)/Images/Dish_Images/"
//let DISH_VIDEO_URL = "\(Server_URL)/videos/Dish_Videos/"

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate


public enum WSRequestType : Int {
    case Register
    case Login
    case UploadMedia
}


struct WebServicePrefix
{
    static func GetWSUrl(serviceType :WSRequestType) -> NSString
    {
        var serviceURl: NSString?
        switch serviceType
        {
        case .Register:
            serviceURl = "Register"
            break
        case .Login:
            serviceURl = "Login"
            break
        case .UploadMedia:
            serviceURl = "UploadingImage"
            break
        }
        return "\(Server_URL)\(WS_PATH)\(serviceURl!)" as NSString
    }
}
