//
//  Enums.swift
//  NIPLFrameworkSwift
//
//  Created by C237 on 07/07/17.
//  Copyright © 2017 developer.nipl. All rights reserved.
//

import Foundation

enum ServiceType:String {
    case GET = "GET"
    case POST = "POST"
    case JSON = "JSON"
    case MEDIA = "MEDIA"
}

enum MediaType:String {
    case IMAGE
    case VIDEO
}

