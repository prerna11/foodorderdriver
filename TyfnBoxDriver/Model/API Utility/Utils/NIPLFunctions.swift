//
//  NIPLFunctions.swift
//  NIPLFrameworkSwift
//
//  Created by C237 on 07/07/17.
//  Copyright © 2017 developer.nipl. All rights reserved.
//

import Foundation
import UIKit

let DEFAULT_TIMEOUT:TimeInterval = 3000.0

class NIPLFunctions
{
    class func showNetworkIndicator(xx:Bool)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = xx
    }
    
    class func getMimeTypeForMediaType(type:MediaType) -> String
    {
        switch type {
        case .IMAGE:
            return "image/jpg"
        case .VIDEO:
            return "video/mp4"
        }
    }
}
