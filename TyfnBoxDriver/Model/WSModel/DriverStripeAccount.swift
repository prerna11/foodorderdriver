import Foundation

public class DriverStripeAccount {
    public var id : String?
    public var object : String?
    public var account : String?
    public var account_holder_name : String?
    public var account_holder_type : String?
    public var bank_name : String?
    public var country : String?
    public var currency : String?
    public var default_for_currency : Bool?
    public var fingerprint : String?
    public var last4 : String?
    public var metadata : NSArray?
    public var routing_number : Int?
    public var status : String?
    
    public var address_city : String?
    public var address_country : String?
    public var address_line1 : String?
    public var address_line1_check : String?
    public var address_line2 : String?
    public var address_state : String?
    public var address_zip : String?
    public var address_zip_check : String?
    public var available_payout_methods : NSArray?
    public var brand : String?
    public var cvc_check : String?
    public var dynamic_last4 : String?
    public var exp_month : Int?
    public var exp_year : Int?
    public var funding : String?
    public var name : String?
    public var tokenization_method : String?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [DriverStripeAccount]
    {
        var models:[DriverStripeAccount] = []
        for item in array
        {
            models.append(DriverStripeAccount(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        
        id = dictionary["id"] as? String
        object = dictionary["object"] as? String
        account = dictionary["account"] as? String
        account_holder_name = dictionary["account_holder_name"] as? String
        account_holder_type = dictionary["account_holder_type"] as? String
        bank_name = dictionary["bank_name"] as? String
        country = dictionary["country"] as? String
        currency = dictionary["currency"] as? String
        default_for_currency = dictionary["default_for_currency"] as? Bool
        fingerprint = dictionary["fingerprint"] as? String
        last4 = dictionary["last4"] as? String
        if (dictionary["metadata"] != nil) { metadata = dictionary["metadata"] as? NSArray }
        routing_number = dictionary["routing_number"] as? Int
        status = dictionary["status"] as? String
        
        address_city = dictionary["address_city"] as? String
        address_country = dictionary["address_country"] as? String
        address_line1 = dictionary["address_line1"] as? String
        address_line1_check = dictionary["address_line1_check"] as? String
        address_line2 = dictionary["address_line2"] as? String
        address_state = dictionary["address_state"] as? String
        address_zip = dictionary["address_zip"] as? String
        address_zip_check = dictionary["address_zip_check"] as? String
        if (dictionary["available_payout_methods"] != nil) { available_payout_methods = dictionary["available_payout_methods"] as? NSArray }
        brand = dictionary["brand"] as? String
        cvc_check = dictionary["cvc_check"] as? String
        dynamic_last4 = dictionary["dynamic_last4"] as? String
        exp_month = dictionary["exp_month"] as? Int
        exp_year = dictionary["exp_year"] as? Int
        funding = dictionary["funding"] as? String
        name = dictionary["name"] as? String
        tokenization_method = dictionary["tokenization_method"] as? String
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.object, forKey: "object")
        dictionary.setValue(self.account, forKey: "account")
        dictionary.setValue(self.account_holder_name, forKey: "account_holder_name")
        dictionary.setValue(self.account_holder_type, forKey: "account_holder_type")
        dictionary.setValue(self.bank_name, forKey: "bank_name")
        dictionary.setValue(self.country, forKey: "country")
        dictionary.setValue(self.currency, forKey: "currency")
        dictionary.setValue(self.default_for_currency, forKey: "default_for_currency")
        dictionary.setValue(self.fingerprint, forKey: "fingerprint")
        dictionary.setValue(self.last4, forKey: "last4")
        dictionary.setValue(self.routing_number, forKey: "routing_number")
        dictionary.setValue(self.status, forKey: "status")
        
        dictionary.setValue(self.address_city, forKey: "address_city")
        dictionary.setValue(self.address_country, forKey: "address_country")
        dictionary.setValue(self.address_line1, forKey: "address_line1")
        dictionary.setValue(self.address_line1_check, forKey: "address_line1_check")
        dictionary.setValue(self.address_line2, forKey: "address_line2")
        dictionary.setValue(self.address_state, forKey: "address_state")
        dictionary.setValue(self.address_zip, forKey: "address_zip")
        dictionary.setValue(self.address_zip_check, forKey: "address_zip_check")
        dictionary.setValue(self.brand, forKey: "brand")
        dictionary.setValue(self.cvc_check, forKey: "cvc_check")
        dictionary.setValue(self.dynamic_last4, forKey: "dynamic_last4")
        dictionary.setValue(self.exp_month, forKey: "exp_month")
        dictionary.setValue(self.exp_year, forKey: "exp_year")
        dictionary.setValue(self.funding, forKey: "funding")        
        dictionary.setValue(self.name, forKey: "name")
        dictionary.setValue(self.tokenization_method, forKey: "tokenization_method")
        return dictionary
    }
    
}
