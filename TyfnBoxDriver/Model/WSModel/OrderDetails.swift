
import Foundation

public class OrderDetails {
	public var order_id : Int?
	public var dish_id : Int?
	public var chef_id : Int?
	public var customer_id : Int?
	public var dish_name : String?
	public var dish_image : String?
	public var order_quantity : Int?
	public var chef_name : String?
	public var customer_name : String?
	public var order_status : Int?
	public var created_at : String?
	public var modified_at : String?
	public var dropp_of_address : String?
	public var dropp_of_postalcode : Int?
	public var dropp_of_city : String?
	public var pickup_address : String?
	public var dropp_of_lat : Double?
	public var pickup_lat : Double?
	public var dropp_of_long : Double?
	public var pickup_long : Double?
    public var chef_phone_num : String?
    public var customer_phone_num : String?
	public var distance : String?
    
    public var dis_btw_dish_customer : Double?
    public var dis_btw_dish_driver : Double?
    public var dis_btw_driver_customer : Double?

    public class func modelsFromDictionaryArray(array:NSArray) -> [OrderDetails]
    {
        var models:[OrderDetails] = []
        for item in array
        {
            models.append(OrderDetails(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {

		order_id = dictionary["order_id"] as? Int
		dish_id = dictionary["dish_id"] as? Int
		chef_id = dictionary["chef_id"] as? Int
		customer_id = dictionary["customer_id"] as? Int
		dish_name = dictionary["dish_name"] as? String
		dish_image = dictionary["dish_image"] as? String
		order_quantity = dictionary["order_quantity"] as? Int
		chef_name = dictionary["chef_name"] as? String
		customer_name = dictionary["customer_name"] as? String
		order_status = dictionary["order_status"] as? Int
		created_at = dictionary["created_at"] as? String
		modified_at = dictionary["modified_at"] as? String
		dropp_of_address = dictionary["dropp_of_address"] as? String
		dropp_of_postalcode = dictionary["dropp_of_postalcode"] as? Int
		dropp_of_city = dictionary["dropp_of_city"] as? String
		pickup_address = dictionary["pickup_address"] as? String
		dropp_of_lat = dictionary["dropp_of_lat"] as? Double
		pickup_lat = dictionary["pickup_lat"] as? Double
		dropp_of_long = dictionary["dropp_of_long"] as? Double
		pickup_long = dictionary["pickup_long"] as? Double
        chef_phone_num = dictionary["chef_phone_num"] as? String
        customer_phone_num = dictionary["customer_phone_num"] as? String
		distance = dictionary["distance"] as? String
        
        dis_btw_dish_customer = dictionary["dis_btw_dish_customer"] as? Double
        dis_btw_dish_driver = dictionary["dis_btw_dish_driver"] as? Double
        dis_btw_driver_customer = dictionary["dis_btw_driver_customer"] as? Double
	}

		

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.order_id, forKey: "order_id")
		dictionary.setValue(self.dish_id, forKey: "dish_id")
		dictionary.setValue(self.chef_id, forKey: "chef_id")
		dictionary.setValue(self.customer_id, forKey: "customer_id")
		dictionary.setValue(self.dish_name, forKey: "dish_name")
		dictionary.setValue(self.dish_image, forKey: "dish_image")
		dictionary.setValue(self.order_quantity, forKey: "order_quantity")
		dictionary.setValue(self.chef_name, forKey: "chef_name")
		dictionary.setValue(self.customer_name, forKey: "customer_name")
		dictionary.setValue(self.order_status, forKey: "order_status")
		dictionary.setValue(self.created_at, forKey: "created_at")
		dictionary.setValue(self.modified_at, forKey: "modified_at")
		dictionary.setValue(self.dropp_of_address, forKey: "dropp_of_address")
		dictionary.setValue(self.dropp_of_postalcode, forKey: "dropp_of_postalcode")
		dictionary.setValue(self.dropp_of_city, forKey: "dropp_of_city")
		dictionary.setValue(self.pickup_address, forKey: "pickup_address")
		dictionary.setValue(self.dropp_of_lat, forKey: "dropp_of_lat")
		dictionary.setValue(self.pickup_lat, forKey: "pickup_lat")
		dictionary.setValue(self.dropp_of_long, forKey: "dropp_of_long")
		dictionary.setValue(self.pickup_long, forKey: "pickup_long")
        dictionary.setValue(self.chef_phone_num, forKey: "chef_phone_num")
        dictionary.setValue(self.customer_phone_num, forKey: "customer_phone_num")
		dictionary.setValue(self.distance, forKey: "distance")
        
        dictionary.setValue(self.dis_btw_dish_customer, forKey: "dis_btw_dish_customer")
        dictionary.setValue(self.dis_btw_dish_driver, forKey: "dis_btw_dish_driver")
        dictionary.setValue(self.dis_btw_driver_customer, forKey: "dis_btw_driver_customer")

		return dictionary
	}

    public func updateOrderStatus(order:OrderDetails,status:Int) -> OrderDetails{
        let newObj = order
        newObj.order_status = status
        return newObj
    }
}
