/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class User {
	public var id : Int?
	public var role_id : Int?
	public var first_name : String?
	public var last_name : String?
	public var username : String?
	public var user_desc : String?
	public var email : String?
	public var facebook_id : String?
	public var hash : String?
	public var password_verify : String?
	public var profile_image : String?
	public var latitude : Int?
	public var longitude : Int?
	public var home_address : String?
	public var phone_number : String?
	public var card_number : String?
	public var name_on_card : String?
	public var expiration : String?
	public var cvv : String?
	public var status : Int?
	public var guid : String?
	public var device_token : String?
	public var device_type : String?
	public var is_verify : Int?
	public var is_delete : Int?
	public var created_at : String?
	public var modified_at : String?
    public var is_stripe_account : Int?
    public var licenseNumber : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Json4Swift_Base Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [User]
    {
        var models:[User] = []
        for item in array
        {
            models.append(User(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Json4Swift_Base Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		role_id = dictionary["role_id"] as? Int
		first_name = dictionary["first_name"] as? String
		last_name = dictionary["last_name"] as? String
		username = dictionary["username"] as? String
		user_desc = dictionary["user_desc"] as? String
		email = dictionary["email"] as? String
		facebook_id = dictionary["facebook_id"] as? String
		hash = dictionary["hash"] as? String
		password_verify = dictionary["password_verify"] as? String
		profile_image = dictionary["profile_image"] as? String
		latitude = dictionary["latitude"] as? Int
		longitude = dictionary["longitude"] as? Int
		home_address = dictionary["home_address"] as? String
		phone_number = dictionary["phone_number"] as? String
		card_number = dictionary["card_number"] as? String
		name_on_card = dictionary["name_on_card"] as? String
		expiration = dictionary["expiration"] as? String
		cvv = dictionary["cvv"] as? String
		status = dictionary["status"] as? Int
		guid = dictionary["guid"] as? String
		device_token = dictionary["device_token"] as? String
		device_type = dictionary["device_type"] as? String
		is_verify = dictionary["is_verify"] as? Int
		is_delete = dictionary["is_delete"] as? Int
		created_at = dictionary["created_at"] as? String
		modified_at = dictionary["modified_at"] as? String
        is_stripe_account = dictionary["is_stripe_account"] as? Int
        licenseNumber = dictionary["license_number"] as? String
	}
		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.role_id, forKey: "role_id")
		dictionary.setValue(self.first_name, forKey: "first_name")
		dictionary.setValue(self.last_name, forKey: "last_name")
		dictionary.setValue(self.username, forKey: "username")
		dictionary.setValue(self.user_desc, forKey: "user_desc")
		dictionary.setValue(self.email, forKey: "email")
		dictionary.setValue(self.facebook_id, forKey: "facebook_id")
		dictionary.setValue(self.hash, forKey: "hash")
		dictionary.setValue(self.password_verify, forKey: "password_verify")
		dictionary.setValue(self.profile_image, forKey: "profile_image")
		dictionary.setValue(self.latitude, forKey: "latitude")
		dictionary.setValue(self.longitude, forKey: "longitude")
		dictionary.setValue(self.home_address, forKey: "home_address")
		dictionary.setValue(self.phone_number, forKey: "phone_number")
		dictionary.setValue(self.card_number, forKey: "card_number")
		dictionary.setValue(self.name_on_card, forKey: "name_on_card")
		dictionary.setValue(self.expiration, forKey: "expiration")
		dictionary.setValue(self.cvv, forKey: "cvv")
		dictionary.setValue(self.status, forKey: "status")
		dictionary.setValue(self.guid, forKey: "guid")
		dictionary.setValue(self.device_token, forKey: "device_token")
		dictionary.setValue(self.device_type, forKey: "device_type")
		dictionary.setValue(self.is_verify, forKey: "is_verify")
		dictionary.setValue(self.is_delete, forKey: "is_delete")
		dictionary.setValue(self.created_at, forKey: "created_at")
		dictionary.setValue(self.modified_at, forKey: "modified_at")
        dictionary.setValue(self.is_stripe_account, forKey: "is_stripe_account")
        dictionary.setValue(self.licenseNumber, forKey: "license_number")
		return dictionary
	}

    public func updateStripe(obj:User, stat:Int) -> User{
        let newObj = obj
        newObj.is_stripe_account = stat
        return newObj
    }
}
